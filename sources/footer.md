
</div>

<footer markdown="1">
Site crée par Mondrak en Markdown, converti en HTML par mes soins. Merci [Framagit](www.framagit.org) pour l'hébergement et [Gitlab](www.gitlab.com) pour les *Gitlab Pages*. Les sources sont disponibles [ici](www.framagit.org/57Ombre101/57ombre101.frama.io).

Me contacter : ombre57101 {at} mailoo {dot} org

Licences, droits... : tout est sous licence totalement libre (CC0). Cependant, je vux bien être averti en cas de réutilisation de tout ou partie de ce site. ^^ 
</footer>
   </body>
</html>
