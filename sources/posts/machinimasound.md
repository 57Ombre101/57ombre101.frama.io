Title: Machinimasound
Date: 2013-04-25 12:00:00
Authors: Mondrak Schwertmann
Tags: musique


Aujourd'hui, je vais vous présenter de la musique (et oui, encore !).

Il s'agit du groupe Machinimasound. C'est un groupe, fondé en 2009 par Jens et Per Kiilstofte, et rejoints plus récemment par Ridvan Duzey. Il font un peu toutes sortes de musique, utilisant FL Studio pour composer tous leurs morceaux, principalement du dubstep, du Hip-Hop et du "symphonique" (que je n'aime pas trop du fait que les instruments soient des échantillons, et ne produisent donc pas les vrais sons. La particularité de ce groupe, c'est que mis à part les pistes payantes, tous les morceaux sont libres et gratuits, car ils sont créés avant tout pour être mis sur des vidéos (de jeux vidéos), sur Youtube principalement.

Ecoutes :

Cloister Of Redemption, morceau épique, dans lequel j'ai l'impression qu'il y a un thème de The Lord Of The Rings.

The Arcade, drum'n'Bass.

Incursion, une musique pouvant aller avec un film type The Avengers.

Lock And Load, "Verrouillé Et Chargé" en français, avec un suspence tout le long du morceau, qui se termine avec le tir.

Queen Of The Night, un morceau pas mal.

The Journey, musique composée pour le jeu Edge Of Space, le morceau que j'ai pu trouver n'est qu'une partie de la musique, que j'ai pu avoir en entier car il a été disponible pendant quelques jours.



A la prochaine !
