Title: Call of Duty : Ghost
Date: 2013-11-14 17:56:00
Author: Mondrak Schwertmann
Tags: jeu vidéo


Aujourd'hui, j'ai eu l'occasion de tester (superficiellement) <strong>Call Of Duty : Ghost</strong>.

J'ai pu remarquer une <strong>amélioration majeure</strong>, qui changera la manière de jouer de tous à Call Of Duty : la <strong>barre de chargement</strong> !

Passons cette petite blague qui reflète un peu la réalité tout de même, j'ai pu voir d'autres points qui changent le jeu. Tout d'abord, moi qui aime pas mal la <strong>personnalisation</strong>, il est mainten ant possible de changer le sexe, le visage... du personnage. C'est plutôt un bon point... Au niveau des <strong>KillStreak</strong>, les <strong>chiens</strong> sont particulièrement ennuyeux. En effet, après avoir tué <strong>cinq ennemis</strong>, vous obtenez un chien qui vous suit, attaque les autres et vous venge s'il est tué. Cela peut être rageant pour les adversaires auxquels il manquait un point pour avoir la "<strong>nuke de l'infini</strong>". Cette série de point permet de tuer tout le monde, et de détruire la carte, et ainsi changer de terrain. Du point de vue des <strong>IAs</strong>, le niveau des "recrues" est plus élevé, je n'ai pas essayé les autres niveaux.

Je n'ai malheureusement pas pu tester toutes les armes et les cartes.

Mis à part ces changements, mais n'étant pas un gros joueur de ces jeux là, je n'ai pas trouvé d'amélioration majeure, à part la barre de chargement qui a été totalement <strong>révolutionnée</strong> ! Je ne pense donc pas que cela vaille vraiment la peine de mettre 70€ dans ce jeu, mais plutôt l'acheter d'occasion, ou avec un pack, ou encore acheter un autre (BO2, MW2...).