Title: 2022 avec Tino !
Authors: Mondrak
	 MxTino
Date: 2017-06-05 14:00:00
Tags: politique


*Nous sommes confrontés à de multiples crises : sociales, économiques, environnementales, politique... Aujourd'hui, en France, ce sont les «élu.e.s» qui sont chargées de les résoudre. Et si on reprenait le pouvoir ? C'est ce que Mxtino propose [ici](https://2017.mrtino.eu/2022). Dans la suite, je reprends ces points, et leur donne un éclairage parfois différent, suivant mes idées.*


[TOC]


## Institutions ##

#### Démocratie liquide ####

Le principe me semble très bien. On élimine les dysfonctionnements de la démocratie causés par le système représentatif, et on augmente pas le risque de *manipulation générale* (démagogie.
Le point noir, c'est clairement la réalisation. Aujourd'hui, le vote électronique est reconnu comme peu fiable, obscur. On pourrait penser à utiliser un système de *blockchain* pour mettre le système en place, mais je ne suis pas un expert sur le sujet.
Dans tous, les cas, il faut que ce soit couplé avec un système efficace qui avertisse des votes en cours, qui donne des moyens de trouver plus d'informations sur le sujet... Mais alors, se pose la question de savoir qui est responsable de cette information. Plutôt que d'avoir un organisme «««objectif»»», chose impossible, s'appuyer sur les informations données par chaque parti. Au citoyen de croiser ses sources. (avec risque d'effet bulle ?)

L'ouverture du droit de votes au résidants en France me paraît bien.


#### Suppression du Sénat et du Conseil Économique, Social et Environnemental dont tout le monde ignore son existence ####

Il faudrait que je me renseigne sur ce que c'est... Vu le système de démocratie liquide, le sénat me paraît caduque.

#### Réduction du nombre de députés à 100 ####

Une fois que les citoyen·ne·s votent les lois, l'assemblée n'a comme rôle principalement que les commissions, chargées de se renseigner sur un sujet, pour fournir des données fiables à son propos, et permettre le vote des lois en accord avec les conclusions de chacun d'après ces données.

La trahison de confiance est pour l'instant assez floue (c'est normal). Ce serait a minima les impératifs légaux en vigueur aujourd'hui (cas de conflits d'intérêts...), mais on pourrait l'étendre aux cas d'assistants parlementaires venant de la famille du député par exemple (à voir le projet de Bayrou sur la question) (*touss touss*).


#### Reconnaissance du vote blanc ####

Beaucoup de votant·e·s blancs sont engagé.e.s. Ce vote blanc a donc une réelle signification. C'est nécessaire de le prendre en compte.

#### Mandat unique ####

Pour les petites communes, peut-être autoriser un maire à faire plusieurs mandats ? Ou peut-être aller jusqu'à deux mandats ? Cinq ans, ça peut être court pour certains projets. Cependant, le système de démocratie liquide permettrait plus de continuité, donc à voir. Par exemple, si læ député·e A à un projet de loi *1*, qui n'est pas finalisé à la fin du mandat, A est en mesure de le promouvoir en tant que citoyen·ne, avec la visibilité acquise en tant qu'ex-député.


#### Droit de vote à partir de 16 ans ####

J'en connais qui aurait aimé voter à l'élection présidentielle, qui étaient renseigné, et qui n'ont pas pu... Les risques de dérive à cause de cette extension du droit de vote me paraissent mineurs.


#### Révision du statut du Président de la République ####

Ici, je peux caser une idée que j'ai eu récemment, a propos du salaire des ex-président·e·s. Pourquoi ne pas laisser chacun·e décider de récompenser, ou non, leur ancien·ne dirigeant·e ? L'idée est assez simple, lors de la collecte des impôts, chacun·e donne un montant pour l'ancien·ne chef·fesse d'état. Ce montant serait plafonné, pour éviter les pots-de-vin promis en amont alors que la personne serait encore en fonction. On peut fixer ce plafond à quelque chose compris entre 1 centime et 1 euro. (Il me manque des données sur le nombre de personnes imposables pour être plus précis·e.)


#### Changement de l'hymne national ####

Ou «Die For Metal» de Manowar ? ;-) (non, ce groupe est trop machiste/nationaliste) Plus sérieusement, je ne suis pas sûr que la Marseillaise soit adaptée à un hymne national (c'est assez militaire/ancien/dépassé). le choix de cet hymne est épineux, il faudrait que ce soit beau avec juste des gens qui chantent sans accompagnement (pas de mélodie trop compliqué). On pourrait mettre l'Internationale, mais c'est peut-être trop politisé.


## Économie ##

#### Lutte féroce contre l'évasion fiscale ####

Clairement oui. Il faut aussi lutter contre les micro-transactions en bourse (une taxation de 1% dessus les réduirait déjà énormément), qui sont un non-sens (liée à rien de concret, juste de l'optimisation fiscale).

#### Abrogation de la Loi El Khomri ####

Cette loi est très impopulaire, et a donné à un important mouvement de résistance. Il ne faut pas abandonner la lutte.


#### Retraite à 60 ans ####

Plus l'âge avance, plus il y a de disparitées entre les personnes en meilleur santé et les autre. On ne peut pas fixer l'âge de la retraite à l'aide de moyenne et d'outils statistiques à cause de ce grand écart-type.


#### Réduire le temps de travail à 32 heures, 30 dans les travaux pénibles ####

Cela fait très longtemps (déjà à l'Antiquité avec les esclaves, puis lors de la révolution industrielle) que l'humanité entrevoit la possibilité de s'émanciper du travail. Le travail disponible se raréfie dans les secteurs manuels (les plus pénibles). Abaisser la charge de travail est sensé. Il serait intéressant d'offrir des heures de formations vers des métiers moins pénible, comme l'informatique, qui est un secteur qui manque aujourd'hui d'employés.


#### Interdiction aux grandes entreprises de licencier ses salariés ####

Sauf en cas de mauvais comportement type aggression sexuelle ? Enfin il y a déjà la justice qui est sensé les punir.


#### Protection des auto-entrepreneurs (Uber, Deliveroo…) ####

On en voit partout en ville, et ces services ont des avantages indéniables, pour le consommateur. Il est temps de protéger ces auto-entrepreneurs, qui ne fondent pas vraiment d'entreprise, mais travaillent pour d'autres... (Il faut que je me renseigne sur les femmes de ménage à domicile, qui sont dans une position analogue, mais depuis plus longtemps.)

**Remarque :** Faute de frappe à auto-entrpreneur


#### Réalisation d'un audit de la dette française ####

Aaah, le spectre financier de la dette, qui sert à promouvoir l'austérité. Pendant longtemps, je me suis demandé comment un état pouvait en arriver à mendier aux banques, des entreprises. Puis j'ai appris un peu plus sur le monde dans lequel nous vivons. Faire plier un état, et son peuple avec, me semble d'une moralité assez douteuse. On pourrait instaurer un facteur de probité, qui allègerait la dette des états non corrompus, faisant des efforts de transparence, démocratiques... Le problème de la dette, c'est que c'est un pays contre le reste du monde.


#### Renégociation des traités européens ####

Sans quitter l'Europe pour autant, il est vrai que certains traités ne sont pas là pour le mieux. Tout le monde pourrait y gagner. Pourquoi ne pas organiser des rencontres régulières des états membre, en vue de corriger et d'harmoniser les régulations existantes, de faire le point sur leur application ?


#### Mise en place de l'impôt universel ####

Ca risque de freiner les gens qui vont à l'étranger pour leurs études, ou qui ont des salaires modestes. Il ne faut pas que ça conduise à enfermer les français non plus.


#### Révolution fiscale ####

Je n'ai pas tout suivi du programme. Il faut que je me renseigne plus, mais ça à l'air valide.



#### Augmentation du SMIC à 1700€ nets ####

En dessous, c'est difficile de vivre correctement.

#### Plafonnement du loyer à maximum 20% du salaire ####

Ca veut dire qu'unæ smicard·e peut payer le loyer d'un grand immeuble de luxe, en tant qu'intermédiaire pour une personne plus fortunée. Il y a donc des risques d'abus.

#### Création d'un million d'emplois dans le service public ####

Où exactement ? L'enseignement, la santé principalement.

#### Renationalisation des autoroutes ####

Oui, les contrats ont été défavorables pour l'état. Diminuer le prix des péages est utile, mais je pense que l'effort devrait être porté sur les véhicules moins polluants, donc par exemple, augmenter le prix du transport par camions.

#### Rejet des traités de libre-échange ####

Les traités CETA, TAFTA... ont été négociés à l'écart des citoyen·ne·s européens. On ne peut pas les accepter en l'état. Il faudrait a minima une rediscussion, et une restriction du champ d'application, voire, préférablement, un abandon pur et simple.


## LGBT ##

#### PMA pour toutes ####

Oui, je ne vois pas de raisons de l'interdire.

#### Légalisation du recours aux mères porteuses ####

Encadrer ça permettrait à la fois de répondre aux attentes des couples qui ne peuvent aps avoir d'enfants, et en même temps garantir de bonnes conditions aux mères porteuses. C'est important de lui garantir des droits, notamment sur l'enfant.

#### Révision de la filiation ####

Oui, mais à partir de quel âge ? A cinq ans, je ne suis pas sûr qu'un enfant ait réellement les capacités de décider. Où mettre la limite ? Difficile à dire.


#### Suppression de la mention du sexe/genre sur les papiers d'identité ####

Totalement d'accord. #teamNonbinary


#### Auto-détermination des personnes intersexe ####

Il y avait un article intéressant sur Libération l'autre jour : [http://www.liberation.fr/france/2017/03/20/etre-reconnu-comme-sexe-neutre-je-le-fais-pour-moi-et-pour-les-autres_1557098]() et la suite : [http://next.liberation.fr/sexe/2017/03/21/ignorer-les-intersexes-c-est-une-mutilation-juridique_1557249]()


#### Lourdes sanctions envers les émissions audiovisuelles porteuses de LGBTQphobies (amendes, interdiction de diffusion et/ou licenciement des personnes concernées) ####

Et s'il n'y avait que Hanouna... [http://tetu.com/2017/06/02/ruquier-dechavanne-cordula-grosses-tetes-roue-libre-transphobe/?utm_source=t.co&utm_medium=referral]()

#### Suppression de la période d'abstinance d'un an avant de pouvoir donner son sang ####

Pas de raison de pénaliser les personnes queer en effet.

## Religion ##

#### Abrogation des dernières lois sanctionnant le culte musulman ####

Pendant ce temps, génération identitaire manifeste avec ses saluts nazi OKLM, en se réfugieant derrière JEANNE©® et le catholicisme comme religion d'état... Les musulmans ne devraient pas avoir à se justifier. Ils sont dans leur droit pour l'exercice de leur religion.

#### Reconnaissance d'aucune religion, l'État est vraiement laïque ####

+1


## Écologie ##

#### Arrêt des subventions des énergies fossiles ####

cf. [Datagueule](http://www.youtube.com/watch?v=aUmJ35kMq1Q)
En plus, si on arrive au point de l'Allemagne, les énergies renouvelables sans subventions seront plus compétitives que les fossiles.

#### Arrêt progressif des centrales nucléaires ####

J'ai retrouvé un bouquin de 1998 : *«Aujourd'hui, l'électro-nucléaire est partout en recul. Seuls la France et le Japon de lui faire pleinement confiance, mais on peut craindre qu'en cas de nouvelle catastrophe (et l'éventualité est loin malheureusement d'en être écartée dans l'ex-URSS, vu la vétusté des installations), il se produirait un brusque et complet retournement de l'opinion, prélude à une réaction de rejet.»*

Quand on voit ce qui est arrivé au Japon, et les vingt ans de plus de nos bonnes centrales, ça n'inspire pas confiance.

#### Sortie du diesel ####

C'est une source de carburant plus nocive que l'essence, donc oui.

#### Gratuité des transports en commun ####

Oui. Aujourd'hui, d'un point de vue économique, les populations les plus pauvres se trouvent souvent loin de leur lieux de travail. Les transports les pénalisent, à cause du temps mis, mais aussi du coût. Par contre, il faut quand même payer les TGVs, transports sur longue distance, mais à moindre coûts.

#### Interdiction des panneaux publicitaires numériques dans les lieux publics ####

Et JCDecaux qui a eu des funérailles nationales l'an dernier. Les politiques qui louaient son esprit d'innovation, le fait qu'il ait donné son empreinte à la ville moderne...

«
Dans un communiqué, le président de la République François Hollande a salué en lui "un grand entrepreneur", qui "a fait de l'entreprise qui porte son nom un groupe l’un des fleurons de l’économie française".

"Jean-Claude Decaux avait la fibre des grands industriels, innovateurs, conquérants. Il a bâti un groupe, une marque, symboles de la France", a réagi le Premier ministre Manuel Valls.

L'ancien président Nicolas Sarkozy lui a rendu aussi un hommage vibrant sur Facebook, saluant "un chef d'entreprise hors pair qui a contribué au rayonnement de la France" et "une source d'inspiration et de courage pour tous les jeunes entrepreneurs".

Le maire de Lyon Gérard Collomb a fait part de son "émotion". "Lyon, où il installa ses premiers Abribus, puis Vélo'v 40 ans plus tard, lui rend hommage", a-t-il tweeté.

» d'après [http://www.la-croix.com/France/Jean-Claude-Decaux-pere-de-l-Abribus-et-roi-de-l-affichage-publicitaire-est-decede-2016-05-27-1300763417]()


## Sécurité et Terrorisme ##

#### Légalisation du cannabis, le cannabis vendu dans les bureaux de tabac est contrôlé par un organisme de santé, taxe de 2% sur chaque paquet vendu ####

Une étude (il faut que je trouve le lien) a montré que le cannabis n'était pas plus nocif que l'alcool. Il faudrait s'inspirer des exemples suisses et scandinaves. Il faut au moins en dépénaliser la consommation.

#### Désarmement de la police municipale et nationale (et des CRS en manifestations) ####

On ne compte plus les yeux perdus...

#### Arrêt des interventions militaires françaises dans le monde et arrêt des ventes d'armes aux pays étrangers ####

Promouvoir des programmes anti-corruption, et une formation des armées étrangères pour qu'elles soient en mesure de lutter seules serait plus efficace.

#### Sortie de l'État d'urgence ####

Urgent depuis un an et six mois...


#### Développer les peines alternatives dans les situations appropriées ####

Et pas faire payer les prisonniers pour leur séjour en prison comme réclame un type de ma région. Il faut aider les délinquants à se réintégrer.

## Justice ##

#### Constitutionnalisation de l'IVG, de la contraception et de l'euthanasie ####

Et de l'égalité des genres tant qu'on y est.

#### Gratuité de l'IVG et de la contraception ####

Ca fera en plus économiser à la sécu sur le traitement des IST.

#### Lutte contre toute forme de discrimination ####

Yay !



## Jeunesse ##

#### Éducation non-sexiste ####

Genderfuck

#### Révision de l'éducation sexuelle ####

Un peu d'éducation sur le viol ne ferait pas de mal. Le corps de l'autre n'est pas un jouet, un instrument librement utilisable.

#### Gratuité des tampons et serviettes comme c'est le cas pour les préservatifs au lycée et Plannings familiaux ####

Pas grand chose à rajouter...


#### Révision à la hausse du montant des bourses du CROUS ####

Il faudrait pouvoir étudier sans être à côté livreur et taxi.


## Internet ##

#### Constitutionnalisation de la Neutralité du Net ####

Surtout si la démocratie utilise Internet. Il ne faut pas que les FAI puissent contrôler les votes. 

#### Favoriser l'emploi de formats libres (Ogg, OpenDocument) dans l'admnistration ####

Enfin, le mp3 est libre maintenant ! ^^ Mais oui, je suis d'accord.

**Remarque :** faute de frappe pour l'«admnistration»


#### Utilisation de systèmes GNU/Linux dans l'administration et dans les écoles/collèges/lycées publics ####

Par contre, pas mettre GNU/Linux dans le futur projet de loi, mais aussi Hurd et les trucs non GNU. Je ne sais pas comment qualifier ça. --> FOSS

#### Suppression du fichier TES ####

Le machin bourré de fautes de frappes, de mauvaises classifications... On ne peut s'en servir pour faire la justice. La seule utilité, c'est pour l'espionnage de masse.

#### Abrogation de la Loi Renseignement ####

Oui, inutile et néfaste.

#### Inciter au chiffrement des communications ####

Comme The Economist le rappelle dans son numéro du 8 Avril, le chiffrement est bon pour l'économie (protège des attaques informatiques). Et on ne peut pas l'affaiblir que pour les terroristes, donc un affaiblissement exposerait **toute** la population. Voir aussi : [http://boingboing.net/2017/06/04/theresa-may-king-canute.html]()

#### Interdire la vente liée (logiciels installés par défaut sur les ordinateurs vendus dans le commerce) ####

Ou bien avoir obligatoirement une alternative libre (pour que Mx Michu puisse avoir un Windows prêt à l'emploi sans trop chercher). Il y a du boulot sur le sujet.

#### Former les personnes âgées à l'utilisation de l'informatique ####

Le gouffre technologique risque de s'amplifier de plus en plus. La cyberprotection est de plus en plus un besoin. Il ne faut pas que les personnes agées soient démunies. Donc il faut les former.

#### Faciliter la création de FAI associatifs ####

Et leur donner priorité sur certains marchés publics (Internet d'une mairie par exemple). De même, il faudrait sensibiliser les gens à la nécessité des FOSS.

## Culture ##

#### Révision du droit d'auteur et du domaine public ####

J'ai lu récemment *Du bon usage de la piraterie* sur le sujet. C'est important que l'art soit accessible, mais cela concerne aussi les connaissances scientifiques. Les brevets nuisent au développement de médicaments à destination des pays les moins développés. Voir plus bas pour la science.

#### Favoriser les licences libres ####

Les instances publiques sont censé fournir du contenu public. Les secteurs concernés sont principalement la recherche, et les entités qui fournissent des données publiques.

#### Interdiction des DRM ####

A cause de ça, c'est moins fatiguant de pirater que d'acheter (véridique, je cherchais à acheter un livre, je suis tombé par hasard sur un lien torrent. L'achat m'a pris un certain temps (pour mettre le bouquin sur le truc d'adobe), le torrent m'aurait pris cinq minutes).

#### Gratuité des musées le dimanche ####

La culture est à tout le monde !

#### Garantir un salaire aux intermittents du spectacle et aux artistes précaires indépendants ####

A voir avec un revenu universel ?

#### Transformer les panneaux publicitaires classiques aux abords des villes et villages en espaces d'expression artistique ####

Ce serait plus sympa !


#### Mettre en place un prix unique sur les albums, les jeux vidéos, les livres et les films ####

Difficile à mettre en oeuvre. Ca risque de favoriser les multiples DLCs et la fragmentation du contenu (du genre, tu paies chaque épisode d'une saison séparément). Il faudrait comparer cette mesure avec des subventions de l'état, ou un plafonnement des prix.


#### Diminuer les prix des articles scientifiques ####

Ce serait bien de s'intéresser aux ouvrages scientifiques, comme les journeaux (*Nature*, *Science*...) gérés par des maisons comme Elsevier qui s'en mettent pleins les poches et qui récupèrent les droits. Certains ouvrages sont aussi prohibitifs (presque 100€ pour un handbook, certe très utile mais bon.

Il serait bien d'obliger les chercheurs du publique à publier sous licence libre. Aujourd'hui, certains sont pour, mais ne peuvent pas car pour faire connaître ses recherches (pour obtenir des financements), il faut publier dans des revues qui prennent les droits. (Aaron Swartz et le cas JSTOR en est un bon exemple.)


#### Augmenter les budgets de la recherche et diminuer la part administrative du travail des chercheurs ####

Les chercheurs passent de plus en plus de temps à chercher des ressources pour pouvoir conduire leurs recherche, car les financements sont faibles, et la paperasse administrative est importante. 


#### Retirer les visas d'exploitation aux films ouvertement homophobes et/ou xénophobes ####

Toute la production française ? C'est un peu exagéré. ^^ Mais ça peut être un bon moyen de lutter contre les discriminations.