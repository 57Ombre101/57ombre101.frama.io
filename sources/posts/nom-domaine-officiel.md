Title: Un nom de domaine "officiel" !
Date: 2013-06-22 11:56:00
Authors: Mondrak Schwertmann
Tags: site

<p>J'ai réservé il y a quelque jours le nom de domaine 57Ombre101.tk. Vous pouvez maintenant accéder au site par cette adresse !</p><p>Cela ne change pas grand chose, mais bon, je vous en ai fait part.</p>