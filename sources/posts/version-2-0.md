Title: Version 2.0 !
Date: 2013-06-27 14:15:00
Authors: Mondrak Schwertmann
Tags: site


C'est aujourd'hui que j'annonce l'ouverture du site 2.0 ! Celui-là, il est fait main. J'ai utilisé pour cela HTML5 et CSS3, ainsi que le PHP (seulement pour les "include"). Actuellement, le site n'est pas encore dans sa version définitive, et www.57ombre101.wordpress.com reste le site principal, jusqu'à nouvel ordre !

N'hésitez pas à commenter !

Vous pouvez le retrouver à l'adresse http://57ombre101.tk/.