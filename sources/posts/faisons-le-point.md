Title: Faisons le point
Date: 2015-04-18 20:29:00
Authors: Mondrak Schwertmann
Tags: site


Bon, ça fait un petit bout de temps que je n'ai rien publié. Pourquoi ? Je rédige plutôt sur smartphone, et ce dernier a eu un problème. De plus, j'ai très peu accès à Internet en ce moment, que ce soit sur mon téléphone ou sur mon ordinateur. Je profite d'être en déplacement pour vous précenir. J'avais de bons articles en préparation, mais j'en ai perdu une partie, et le reste n'a pas pu être continué.

Pour ces raisons, je met ce blog en pause. J'espère pouvoir y retourner bientôt cependant. 

Voilà,&nbsp; vous savez tout.(Pour plus d'informations, veuillez me contacter personnellement sur 57ombre101@gmail.com ou sur mon compte Twitter)

EDIT du 2015-06-16 : Je reprends comme je peux. Cependant, j'entre en prépa l'année prochaine et ne pourrai vraisemblablement guère écrire.