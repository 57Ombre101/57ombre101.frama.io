Title: Que Charlie vive !
Date: 2015-01-09 22:20:00
Authors: Mondrak Schwertmann
Tags: charlie hebdo
      liberte

Ce ne sera pas un long discours que je ferai là. D'autres s'en chargeront bien mieux que moi. Cependant, je veux rendre hommage aux douze victimes de l'attentat de Charlie Hebdo. Je rend hommage à la policière tuée à Montrouge. Je rend hommage aux quatre victimes des prises d'otages d'aujourd'hui.

Cependant, après le deuil, il faut de nouveau s'intéresser au monde, aux autres. En France, la liberté de la presse a été touchée, mais pendant ce temps, Boko Haram commet son pire massacre ; en Russie, les gays et trans sont discriminés. C'est pourquoi nous devons rebondir, et participer activement à la défense de la liberté.

Si j'ai écris cet article, c'est pour montrer que je n'ai pas peur des terroristes. Je défends la liberté et condamne ces actions atroces. Si vous aussi vous pouvez écrire, dessiner, sculpter, chanter pour la liberté, n'hésitez pas, allez-y.

Edition du 2015-01-10 : Retrouvez <a href="http://tyztornalyer.tumblr.com/post/107444848014/en-2013-alors-que-javais-encore-lhabitude-de">ici</a> un article sur le même thème de TyzTornalyer.