Title: Musique en vrac (Ouroboros, Le Pré Où Je Suis Mort, Motionless In White)
Date: 2015-07-29 12:09:00
Authors: Mondrak Schwertmann
Tags: musique
      metal

Bonjour,

Ça faisait longtemps que je n'avais pas parlé de musique. Dans cette rubrique, je compte parler de quelques morceaux en particuliers, venant de différents artistes.

Je commence fort avec le trash melodic du groupe australien Ouroboros. C'est un groupe encore jeune qui produit des mélodies de qualité. Je ne suis pas fan de trash, mais les riffs présentent ici une technicité que j'apprécie. Le groupe a sortie son second album cette année mais je ne l'ai pas écouté. Le morceau que je vous présente est Lashing Of The Flames, issu de l'album Glorification Of A Myth (le premier). Il commence de manière mélodique, avec un gros côté trash, et continue ainsi jusqu'au solo. Là, le guitariste est en mesure de montrer l'étendue de sa virtuosité. Ensuite, une guitare accoustique vient tempérer l'ensemble et offre une pose à l'auditeur. Puis c'est le final, avec un retour du style très trash qui contraste terriblement avec l'intermède calme.

Vidéo (non officielle) : <a href="www.youtube.com/watch?v=XEYk71RZJwg">m.youtube.com/watch?v=XEYk71RZJwg</a>


Nous venons d'écouter un Metal exubérant, place maintenant à du post-hardcore plus léger avec le groupe franco-suisse Le Pré Où Je Suis Mort. C'est un groupe qui a disparu (leur dernière sortie date de 2007 et il n'y a pas de nouvelles) me semble-t-il, après avoir sorti un EP et quelques chansons dans une compilation. Ce que j'aime bien, c'est que c'est du Screamo qui réussi aussi à installer un côté rêveur et doux. <a href="http://www.metalorgie.com/groupe/Le-Pre-Ou-Je-Suis-Mort">Certains</a> ont aussi parlé d'un côté Rimbaldien et je suis d'accord. Ce groupe marie la flamme et la douceur à merveille. J'ai décidé de vous faire partager le morceau Une Fois De Plus. Il commence assez calmement, avant de s'envoler avec le scream du chanteur. Puis, le morceau alterne, oscille entre la guitare plus paisible et le chant révolté. La fin vient et dure sur un long decrescendo. Bref j'adore.

<a href="ww.youtube.com/watch?v=oWjlPh80lLI">m.youtube.com/watch?v=oWjlPh80lLI</a>


Bon, enchaînons avec Motionless In White, du metalcore gothic que j'adore. C'est un groupe mené par Chris Motionless accompagné de musiciens talentueux. Certaines chansons sont engagées, ce qui n'est pas pour me déplaire. Ils ont sorti un nouvel album nommé Reincarnate en début d'année, que je n'ai pas encore. Je l'ai cependant écouté sur YouTube. De là, je vous ai sorti une chanson : Break The Cycle. Comme dans la plupart des chansons du groupe, il y a une alternemance d'usage de voix claires sur les refrains et de growl sur les couplets. Les premières notes suffisent à instaurer une atmosphère mystérieuse et tendue. La mélodie est entraînante et bref, j'aime bien. C'est construit de manière moins linéaire que les deux chansons que je vous ai présentées au-dessus. Je vous conseille aussi de regarder le <a href="www.youtube.com/watch?v=1gZgqh-135g">making-of</a> du clip.

<a href="m.youtube.com/watch?v=atzrIixEKZM">www.youtube.com/watch?v=atzrIixEKZM</a>

C'est tout pour cette fois. Si vous avez des suggestions à me proposer, n'hésitez pas !