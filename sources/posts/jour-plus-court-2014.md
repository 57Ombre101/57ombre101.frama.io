Title: Le jour le plus court, cru 2014
Date: 2014-12-23 18:25:00
Authors: Mondrak Schwertmann
Tags: film
      court-metrage

Le <strong>21 décembre</strong> est le jour le plus court de l'année. C'est pour cette raison que la journée du <strong>court-métrage</strong> est organisée en cette date. Cela fait trois ans que je me rend cette date à une projection dans ma commune. L'année dernière, j'avais pu découvrir pas mal de choses (comme <strong>Bad Toys 2</strong> que je vous recommande de regarder sur YouTube), et cette année encore, j'ai retenu quelques noms importants. Mais je n'ai pas pu assister à une des deux séance, d'où une liste un peu courte.

Tout d'abord, <strong>Blue Line</strong> est un court-métrage dont l'histoire se déroule à la frontière Liban-Israël. Un jeune berger emmène sa vache boire dans un étang, mais celle-ci s'avance dans le plan d'eau, et finit par passer la frontière, surveillée d'un côté par un poste de l'ONU, où se trouve un Indien, et un poste israélien, redoutant une attaque du Hezbollah dans la journée. Ne pouvant franchir la ligne bleue, c'est tout une aventure pour sauver la vache qui commence... 
J'ai bien aimé ce court-métrage, car il parle l'actualité tout en amenant un peu d'<strong>espoir</strong>. Cet espoir est néanmoins ténu, comme le montre la dernière scène. Il est assez <strong>réaliste</strong>, et permet de mieux comprendre la situation à cette frontière. Pour le regarder, c'est ici : [www.youtube.com/watch?v=RYOfHYdvgv8](www.youtube.com/watch?v=RYOfHYdvgv8)

Ensuite, dans un tout autre genre, il y a <strong>Voisin Voisin</strong>. C'est l'histoire de deux voisins, qui soudain ne reçoivent plus l'électricité. L'un aime les chats, l'autre écrit des livres d'espionnage clichés (guerre froide, espionne du KGB tombe amoureuse d'un américain...). 
Pourquoi ai-je aimé ce film ? Il dst totalement <strong>déjanté</strong>, les personnages sont de grands enfants, bref, c'est ce que j'aime. Il leur arrive des aventures irréalistes qui permettent d'aborder plusieurs facettes des personnages. Si vous pouvez, regardez-le, c'est à cette adresse : www.dailymotion.com/video/x17i3b2_voisin-voisin_fun.

Comme toujours, la discussion est ouverte !