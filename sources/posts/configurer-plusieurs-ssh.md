Title: Utiliser plusieurs clefs SSH
Authors: Mondrak Schwertmann
Date: 2018-02-12 20:00:00
Tags: SSH
      Informatique

Il peut être très utile d'utiliser plusieurs clefs **SSH** sur un même appareil.
En effet, cela permet de renforcer la sécurité (une clef compromise n'impacte pas tous les services utilisés), ou de bien séparer plusieurs identités (professionelle, privée...) par exemple.
Seulement, ça ne se fait pas automatiquement, et il faut configurer un peu le système.
Je ne fais ici presque que recopier (et traduire) la solution à ce post sur [StackOverflow](https://stackoverflow.com/questions/23751625/how-to-manage-multiple-ssh-keys-in-the-ssh-directory).

#### Génération de la clef ####

On va commencer par générer une clef SSH.
Pour cela, on [choisit l'algorithme](https://blog.adminrezo.fr/2016/01/comment-choisir-sa-cle-ssh-rsa-dsa-ecdsa-ed25519/).
Par défaut, SSH propose du RSA sur 2048 bits.
Aujourd'hui, on préfèrera utiliser des clefs ECC.
ECDSA étant possiblement troué par la NSA, on préfèrera Ed25519 (Désolé pour ces noms barbares).
On lance donc

```
ssh-keygen -t ed25519 -C "Comment"
```

L'option `-t` est utilisée pour spécifier l'algorithme choisi.
`-C` est facultative est permet de spécifier un commentaire, la valeur par défaut est `user@device`.
Ed25519 n'étant pas supporté par toutes les machines, vous pourriez avoir à utiliser une clef RSA à la place, avec `-t rsa`, et en ajoutant `-b 4096` pour avoir plus de sécurité que 2048 bits.
L'algorithme DSA est maintenant vulnérable et ne doit plus être utilisé (et n'est donc plus pris en charge par OpenSSH 7.0 et supérieur).

#### Stockage des clefs ####

Quelques précautions sont à prendre lors du stockage des clefs.
On peut s'organiser de plusieurs manières, toutes les stocker directement dans le `~/.ssh`, ou faire des sous-dossiers à l'intérieur.
Cependant, il faut accorder les bonnes permissions (pour des raisons de sécurité, étant donné que la clef privée ne doit pas être lisible par n'importe qui).
Pour le(s) dossier(s) parent(s) des clefs, il ne faut pas accorder de droit en écriture.

```
chmod oa-w dossier
```

Ensuite, on configure les droits pour les clefs elles-mêmes.
Ce sera 644 pour les clefs publiques, et 600 pour les clefs privées.

```
chmod 600 clef

chmod 644 clef.pub
```

#### Configuration de ~/.ssh/config ####

On édite (ou on crée) `~/.ssh/config` :

```
Host nom-d-hote

    User utilisateur
    
    IdentityFile ~/.ssh/arborescence/clef
    
    IdentitiesOnly yes
```

Par exemple, pour configurer une clef à utiliser avec git, on écrit `User git`.

#### Configuration de git ####

Il ne reste qu'à configurer le dépôt **git** !
Si vous n'avez pas encore de *remote* configuré, et que votre dépôt se trouve par exemple à gitlab.com/arborescence/repo (de même avec Github) :

```
git remote add origin nom-d-hote:arborescence/repo.git
```

Si une branche est déjà configurée,

```
git remote set-url origin nom-d-hote:arborescence/repo
```

Et voila, vous pouvez maintenant utiliser plusieurs clefs **SSH**, notamment avec **git**.
