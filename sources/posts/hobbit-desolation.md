Title: Le Hobbit : La Désolation
Date: 2013-12-14 22:36:00
Authors: Mondrak Schwertmann
Tags: film
      critique

Vendredi soir, salle de cinéma, lunettes 3D check...

Début de la projection de <strong>Le Hobbit : La Désolation De Smaug</strong>, je savais à quoi m'attendre, ayant vu le premier, mais cette fois, les records étaient de nouveau atteints.

Premièrement, les elfes avaient pour mot d'ordre "<strong>invincibilité</strong>". 1 contre 60 orcs, pas de problèmes. Sur une rivière, tirer des flèches à travers la tête hideuse d'un ennemi pour atteindre son camarade, facile ! Bref, Legolas et Tauriel sont beaucoup trop mis en avant de par leurs capacités un peu trop forcées, mais ce ne sont pas les seuls combats de ce type.

Par exemple, le <strong>tonneau</strong> de Bombur heurte la rive, et dévale alors la pente sur une bonne centaine de mètres, tuant moult orcs au passage. Coïncidence ? Je ne pense pas. De plus, il se relève, et vainc plusieurs orcs, toujours en restant dans le tonneau.

Finalement, même s'il n'y avait que cela, le film ne serait pas mauvais. Cependant, je n'aime pas le fait qu'ils aient repris le titre de l’œuvre de <strong>J.R.R. Tolkien</strong>, car il y a tellement d'ajouts, et l'atmosphère est si différente que je trouve que le film de <strong>Peter Jackson</strong> n'a rien à voir avoir le livre.

<span style="text-decoration:underline;">Note :</span> <strong>5</strong>/20, pour la qualité de l'environnement reconstitué.

<span style="text-decoration:underline;">Note musicale :</span> <strong>9.5</strong>/20, je n'aime pas trop les musiques du Hobbit, qui ressemblent trop au musiques standards des autres films.

<span style="text-decoration:underline;">Remarque :</span> Je vous conseille plutôt de <strong>lire</strong> le livre, qui est <strong>excellent</strong> et pas trop long pour ceux qui n'aiment pas trop lire.