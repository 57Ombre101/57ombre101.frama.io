Title: Printemps Calme, le retour
Date: 2013-06-01 12:00:00
Authors: Mondrak Schwertmann
Tags: musique


Salut à tous !

En ce début de Juin, j'annonce l'ouverture de ma chaîne Youtube ! Et surtout, que "Printemps Calme" est maintenant disponible sur la célèbre plate-forme de vidéos.

[youtube=http://www.youtube.com/watch?v=wvD2m8fI1Mk]

...Et aussi sur Soundcloud !

[soundcloud url="http://api.soundcloud.com/tracks/92774944" params="" width=" 100%" height="166" iframe="true" /]

Le prochain morceau de l'album, le morceau phare, "The 1000th Run", sortira d'ici peu de temps !

A bientôt !
