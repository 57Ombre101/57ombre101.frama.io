Title: Jaune Flux, de la musique inhabituelle
Date: 2014-11-09 20:18:00
Authors: Mondrak Schwertmann
Tags: musique
      jaune flux

Récemment, mon père m'a fait découvrir <strong>Jaune Flux</strong>. C'est un groupe de... rock/électro/world/random qui mérite un peu d'attention.

Je vais commencer par vous parler de <a title="Revolver" href="https://soundcloud.com/jauneflux/revolver" target="_blank">Revolver</a>. Le morceau débute par quelques secondes de calme, bientôt brisé par des percussions électroniques. Petit à petit, le rythme s'installe, jusqu'à un passage avec un sitar (instrument à cordes pincées utilisée en Asie), rejoint par une guitare. Ensuite, la voix de la chanteuse s'élève, modifiée un peu par un echo. Les paroles des chansons sont généralement en français, voire en anglais, mais la, c'est du français. La musique se poursuit, dans des tons aériens, aux sonorités assez inutilisée en occident actuellement.

Ensuite, voici <a title="20th century storm" href="https://soundcloud.com/jauneflux/20th-century-storm" target="_blank">20th century storm</a>. Le morceau est un peu plus classique (et encore !), et là, c'est <strong>un</strong> chanteur ! Le morceau est assez calme et un peu triste. Des sortes d'échos à la guitare électrique ajoutent de la profondeur. Je ne me suis guère intéressée aux paroles, mais elles sont poétiques reflètent donc le groupe, qui a un univers assez onirique.

Bref, <strong>Jaune Flux</strong> est un bon petit groupe français. Leur musique vous <strong>dépaysera</strong>. Ils utilisent des <strong>instruments rares</strong> voire même uniques à voir sur leur site web). De plus, la totalité de leurs chansons sont téléchargeables <strong>gratuitement</strong>sur Soundcloud. Alors n'hésitez plus, écoutez !

Site Internet :http://www.jauneflux.com/

Soundcloud : https://soundcloud.com/jauneflux