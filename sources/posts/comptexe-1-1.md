Title: ComptExe v1.1
Date: 2013-11-10 21:05:00
Authors: Mondrak Schwertmann
Tags: jeu vidéo
      calculatrice


Bonjour à tous !

Aujourd'hui, je vais vous présenter un programme que j'ai réalisé pour une calculatrice Casio Graph60. Le nom de ce programme est ComptExe !

Ce programme est un Cookie-Clicker-like, c'est à dire qu'il vous faut appuyer le plus possible sur la touche Exe de la calculatrice. Cependant, j'ai ajouté un bonus, vous permettant de gagner +1 pour chaque clic et par nombre de fois que vous avez acheté le bonus. Le prix de ce bonus augmente un peu à chaque achat, améliorant un peu plus la difficulté.

J'ai prévu plusieurs améliorations. Je commencerai par ajouter un bonus multiplicateur, et une page de magasin, pour ne pas surcharger l'écran de jeu. Ensuite, je rajouterai un troisième, et dernier bonus !

Ce petit programme m'a permis de m'initier au "Getkey", indispensable à la création de jeux sur Casio. Il m'a aussi permis de me remettre dans le bain de la programmation sur calculatrice, que j'avais un peu délaissée. J'espère vous livrer d'autres programmes plus tard !

Voici le code source : https://docs.google.com/document/d/13VRkrwewiZlbFyscDR7a2YPNQcEdQ_56UHraVZ6hap0/edit?usp=sharing

Je mettrai ce document à jour à chaque nouvelle version.

Pour plus de renseignements sur la programmation sur Casio, rendez-vous sur <a title="Tutoriel Basic Casio" href="http://fr.openclassrooms.com/informatique/cours/apprenez-le-basic-casio" target="_blank">OpenClassrooms</a> (anciennement Site Du Zéro) et sur <a title="Planet-Casio" href="http://www.planet-casio.com/Fr/" target="_blank">Planet-Casio</a> (la section Programmation propose un <a title="Planet-Casio Tutoriel" href="http://www.planet-casio.com/Fr/programmation/" target="_blank">tutoriel</a>) !

Si vous avez des questions, n'hésitez pas à me les poser !