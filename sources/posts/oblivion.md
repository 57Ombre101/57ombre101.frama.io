Title: Oblivion
Date: 2013-04-15 12:00:00
Authors: Mondrak Schwertmann
Tags: film



Dimanche 14 Avril, je suis allé voir Oblivion [lien wikipédia](https://fr.m.wikipedia.org/wiki/Oblivion_(film))). C'est un film futuriste, avec Tom Cruise, des drones, une Terre dévastée...


Avant de vous donner ces impressions, je n'ai vu que le film, pas de critiques, avis... A partir de maintenant, des événements du film seront cités, ne lisez pas plus loin si vous voulez le voir bientôt...



C'est un film typique du genre anticipation Hoolywoodienne. Le héros est dans le camps des méchants, il comprend que ce n'est pas le bon, le quitte, les gentils sont moins forts que leurs ennemis, une histoire d'amour avec une jalouse, un gentil qui meure, mais pas le principal...


Un gros problème subsiste pour moi, pourquoi avoir appelé ce film "Oblivion" ? Ils ne disent jamais ce mot, il doit sûrement signifier quelque chose, mais malgré de courtes recherches, je ne l'ai pas trouvé.


Sur la musique, que j'ai trouvée "fade", pas extraordinaire, j'ai trouvé un bon élément : au début, le héros est à l'intérieur (le spectateur le voit par une baie vitrée), la musique est plutôt piano. Lorsqu'il ouvre la porte, il y a un crescendo très rapide, pour arriver sur un forte.


Il y a des incohérences (moi et mes copains savons très bien les repérer) : la maison du héros est à au moins dix-mille mètres d'altitude, et pourtant, il respire très bien à l'extérieur...


Enfin, j'aime bien quand les univers sont développés, et là, je trouve que ça manque d'explications, de flash-backs sur la guerre qui s'est passée sur Terre, sur le Thètraèdre...


####Conclusion :


Le film est un genre de film standard, qui se rapproche de Battleship à mon goût. Allez le voir si vous voulez. Il dure deux heures, et un de mes amis préfère les films longs (plus rentables car ils coûtent le même prix !), c'est donc de ce genre de film (Oblivion dure deux heures) dont il parle.
