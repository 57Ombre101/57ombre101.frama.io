Title: The Boy Who Could Change The World
Date: 2016-11-25 9:00:00
Author: Mondrak Schwertmann
Tags: informatique
      lecture

J'ai récemment lu, un excellent livre : <strong>The Boy Who Could Change The World</strong>. C'est un ouvrage particulier, car c'est une compilation d'articles écrit par <strong>Aaron Swartz</strong> – au sujet duquel j'ai déjà posté un petit truc ici – principalement pour son blog, mais aussi quelques autres sites. Tous ces fragments ont été rassemblés par de ses connaissances, qui commentent les différentes parties composant le livre.
Pourquoi ai-je aimé cet écrit ? Parce que le contenu est à la fois assez divers et très intéressant. En effet, il est divisé en six sections : <em>Free Culture</em>, <em>Computers</em>, <em>Politics</em>, <em>Media</em>, <em>Books and Culture</em>, et <em>Unschool</em>, sur lesquelles je vais revenir une par une.

####Free Culture

Aaron traite des questions d'accès à la <strong>cultur</strong> – à la fois scientifique et artistique. En effet, la culture est un bien qui du fait de la technologie actuelle, pourrait être accessible à tous, à peu de frais. C'est pourquoi Aaron est contre le <strong>copyright</strong>, qui empêche cet accès. Il propose alors des solutions, peut-être imparfaites, mais qui mériteraient néanmoins un réel intérêt de la part des législateurs. De plus, Aaron est un contributeur de <strong>Wikipédia</strong>, qui a candidaté au conseil d'administration de la fondation. Ce fut l'occasion d'écrire à propos de cette association : qui y contribue ? Qui y décide ? Comment l'améliorer ? Ces recherches ont eu un impact significatif, car elles ont révisées d'anciennes pensées sur Wikipédia.

####Programming

C'est sûrement la partie la moins intéressante pour legens non attirés par l'informatique pure. Cependant, Aaron écrit tout de même des articles pertinents, mais aussi parfois corrige ses opinions passées. En effet, il défend tout d'abord un <em>web sémantique</em>, adapté aux machines et leur permettant de récolter des informations efficacement. Il se rétracte par la suite, expliquant qu'aujourd'hui, les machines arrivent de mieux en mieux à «interpréter» les données en langage humain. Il explique aussi pourquoi il est préférable de <em>bake</em> plutôt que <em>fry</em>, c'est-à-dire qu'il faut mieux qu'un programme créé des pages statiques sur le serveur, plutôt que d'avoir à régénérer les pages pour chaque client.


####Politics

Bien qu'assez centrée sur le système étasunien, ce qui est écrit ici peut aussi s'appliquer à la France. De plus, cela permet de bien comprendre les mécanismes en jeu aux États-Unis d'Amérique, pratique en ces temps d'élection. Aaron explique la manière de devenir député, ce qu'est le parti républicain, et ses <em>think-tank</em>. Il a visiblement fait des recherches sur ces sujets, et justifie bien ses dires.


####Media

Aaron à très à cœur la diffusion du savoir dans son ensemble, c'est-à-dire scientifique, mais aussi les actualités. Il parlent ainsi beaucoup des médias. Notamment, il explique comment la notion d'objectivité est apparue assez récemment. Sous le couvert de cette valeur que seraient sensés respectés les journalistes, ces derniers diffusent encore leurs opinions. Or, elles sont alors moins sensibles à l'esprit critique, car le journaliste étant objectif, il ne peut dire que la vérité.
Il montre aussi que la façon de traiter le changement climatique et les problèmes environnementaux par les média a été la cause d'une mauvaise information, notamment au sujet du DDT.


####Books and Culture

Aaron est un grand lecteur. Il lit beaucoup de textes politiques, économiques ou philosophique, et les partages à son tour, analysant en détail les points améliorables, et n'hésitant pas à dire clairement ses pensées.


####Unschool

A mon sens, c'est la partie la plus intéressante, extrêmement instructive. Aaron s'attaque à la racine des problèmes : l'<em>éducation</em>. Vraiment, lisez au moins cette section, appuyée par de nombreuses références scientifiques.
Pour commencer, Aaron explique que les enfants, et les nourrissons, sont extrêmement curieux. Ensuite, il parle de l'école, et notamment de la création de cette institution aux États-Unis Et ceci est important, car en effet, il apparaît que l'école a d'abord été créée lors de la révolution industrielle afin de subordonner les futurs ouvriers. Pour en savoir plus, lisez le reste, mais sachez que ce chapitre est dense en faits importants pour comprendre l'éducation. Pour approfondir, vous pouvez regarder <a href="m.youtube.com/watch?v=FR0_sZtCfJ0">cette vidéo de Boyinaband</a>.


<strong>Conclusion</strong>

Ce livre est génial. Non seulement Aaron est un <em>informaticien</em>, au sens large, de génie, mais il s'est intéressé à beaucoup de sujets importants afin de trouver des solutions, dans son objectifs de réparer le monde. D'abord centré sur des problématiques informatiques, il a élargi ses perspectives pour finalement analyser une grande variété de sujets. En particulier, l'éducation, période qui pour lui a été synonyme de peur et d'ennui, a reçu de sa part beaucoup d'intérêt. Il est alors remonté à la source, et a décortiqué le procédé pour en trouver des solutions : l'<em>unschool</em> et les pédagogies centrées sur l'enfant et <strong>son</strong> apprentissage.
