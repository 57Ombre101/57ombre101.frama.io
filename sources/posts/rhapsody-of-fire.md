Title: Rhapsody Of Fire
Date: 2013-04-19 12:00:00
Authors: Mondrak Schwertmann
Tags: musique


Salut à tous !

Aujourd'hui, je vais vous parler d'un groupe de Metal que j'aime beaucoup : Rhapsody Of Fire ! C'est un groupe de Metal Symphonique, plus précisément, fondé à Trieste, en Italie.

Dawn Of Victory est le premier morceau du groupe que j'ai entendu. Les éléments du Metal cohabitent avec des éléments classiques.

[youtube=http://youtu.be/0L_iOnLNt9M]

j'aime bien le pianiste Alex Staropoli. Il a l'air pas mal impliqué dans ce qu'il fait. Donc, c'est un groupe de Metal, qui utilise de instruments Metal, mais pas que... Dans The Magic Of The Wizard's Dream, le groupe joue avec un orchestre, et avec Christopher Lee (qui incarne Count Dooku, Saruman...), qui fait aussi la narration dans de nombreux albums. Il a une merveilleuse voix de basse !

[youtube=http://youtu.be/Z93SdirnzTw]

Je vous invite à écouter d'autres morceaux du groupe. Et pour finir en musique :

[youtube=http://youtu.be/AQVEAPjKae4]

Pour en écouter plus : http://grooveshark.com/#!/artist/Rhapsody/29375.
