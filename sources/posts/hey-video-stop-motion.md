Title: Hey ! Une vidéo en stop-motion.
Date: 2013-06-23 12:27:00
Authors: Mondrak Schwertmann
Tags: court-metrage


Bonjour à tous !

Aujourd'hui, je met en ligne une vidéo de stop-motion Lego. C'est moi qui ai créé la musique, et j'espère que vous l'apprécierez !

Au départ, ceci n'était qu'un essai de stop-motion Lego, pour faire quelque chose de plus grand... Mais, j'ai monté les images, regardé le rendu, et me suis dit que je pouvais essayer d'ajouter une bande-son. Après quoi, je me suis dit que le rendu final pouvait être montré à tous !

[youtube=http://youtu.be/ZVg4OimFeC4]

[soundcloud url="http://api.soundcloud.com/tracks/98078507" params="" width=" 100%" height="166" iframe="true" /]