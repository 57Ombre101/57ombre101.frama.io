Title: Kill La Kill
Date: 2014-12-19 19:38:00
Authors: Mondrak Schwertmann
Tags: anime

Je viens de finir les 24 épisodes de <strong>Kill La Kill</strong>, et je pense qu'il est assez important que je vous en parle. Ne vous inquiétez pas, je ne révèlerai rien d'important sur l'histoire.

Kill La Kill est le premier anime du studio <strong>Trigger</strong>, et bien qu'il ait parait-il fait face à des difficultés financières, il est très bien réalisé. Je ne suis pas très connaisseur en anime (principalement Kill La Kill et One Piece), mais celui-ci a de belles qualités.

Kill La Kill, c'est l'histoire d'une fille nommée Matoi Ryuko. Elle intègre lors du premier épisode l'Académie Honnouji, où elle cherche le meurtrier de son père. Armée d'un ciseau et d'un vêtement vivant, capable de se transformer en puissante armure de combat (mais qui ne couvre que très peu le corps...), elle se sent capable de chercher à assouvir sa vengeance. Se faisant, Ryuko rencontrera sur son chemin de nombreuses personnes étranges, aux tétons lumineux violets notamment, amis ou ennemis. 

En premier lieu, intéressons-nous au <strong>scénario</strong>. L'histoire est bien <strong>cohérente</strong>. Selon les hypothèses de départ, les fibres vivantes, tout le reste est plausible. La fin est tout à fait réaliste et "bonne". Le rythme de chaque épisode est à peu de choses près parfait : l'histoire avance un peu, il y a généralement un moment pour souffler, et une bonne baston.
Les <strong>personnages</strong> sont tous intéressants. Il y a d'un côté la masse, des "porcs en uniformes" comme le dit Kiruyn Satsuki, et de l'autre, les personnages principaux et secondaires. Parmis ces derniers, leur histoire est un peu expliquée, ajoutant de la profondeur.
Ensuite, les <strong>combats</strong> sont bien faits : nerveux, riches en couleurs, bien rythmés. Bref, il n'y a pas grand chose à redire là-dessus. Ils respectent bien l'univers de Kill La Kill.
La <strong>musique</strong> est adaptée à chaque situation. Elle va de la musique avec des cuivres lorsque l'héroïne arrive pour sauver les gentils, à la musique hypnotisante de la mère de Kyruin Satsuki. C'est de la musique d'anime japonais comme je les aime.
Une subtilité de Kill La Kill réside dans le fait que les scénaristes ont <strong>joué sur les mots</strong>. En effet, en Japonais, <em>fasciste</em> se prononce d'une manière similaire à <em>fashion</em>. Il en va de même pour <em>uniforme scolaire</em> et <em>conquète</em>, ainsi que pour <em>tuer</em> et <em>porter</em> et <em>couper</em>. Ainsi, Kill La Kill se dresse comme une <strong>métaphore de notre société</strong>, régie par des codes vestimentaires.

Pourquoi est-ce que je vous conseil Kill La Kill ? Parce qu'il s'agit d'un véritable moment de <strong>détente,</strong> animé d'un bon humour, par des personnages <strong>originaux</strong>, hauts en <strong>couleurs</strong>. De plus, cet anime possède une certaine <strong>profondeur</strong>, qui se dévoile notamment vers l'épisode 17 et se poursuit pour un final flamboyant. Cependant, cet anime ne convient pas au plus jeunes (encore que...).

<em>Source : Wikipedia</em>

P.S. : N'hésitez pas à commenter !