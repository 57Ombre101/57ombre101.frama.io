Title: Printemps Calme
Date: 2013-05-18 12:00:00
Authors: Mondrak Schwertmann
Tags: musique


Hey ! Salut à tous !

Aujourd'hui, voici venu le temps de vous montrer ma première création musicale : "Printemps Calme" ! Ce morceau a été composé avec Sunvox, sur Android. Le morceau n'a pas été très approfondi - je n'ai pas cherché à utiliser moults effets, mais plutôt a prendre en main l'application. J'espère que vous l'aimerez !

[soundcloud url="http://api.soundcloud.com/tracks/92774944" params="" width=" 100%" height="166" iframe="true" /]
