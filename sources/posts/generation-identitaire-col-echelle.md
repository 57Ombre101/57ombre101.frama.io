Title: Génération Identitaire au col de l'Échelle
Authors: Mondrak
Date: 2018-04-23 22:00:00
Tags: politique

Le samedi 21 Avril de l'an de grâce 2018, environ une centaine de membres de Génération Identitaire sont monté au col de l'Échelle, dans les Alpes, afin d'y installer des banderoles et des filets pour manifester leur haine anti migrant·e·s.
On aurait pu pensé, au vu de l'activité récente de la police qui se plaît à aller cogner les manifestations, qu'il allait y avoir des réactions de la part de nos charmant·e·s policièrs.
Et en effet, il y a eu des arrestations !
Six personnes qui ont fait passer des migrant·e·s dimanche ont été placé en garde à vue !
Six antifas qui souhaitaient aider des personnes dans le besoin ont subi la répression policière.

Alors que Gérard dit condamner autant «l'ultra-droite» et «l'ultra-gauche», il semblerait que dans les faits, le gouvernement supporte activement les milices d'extrèmes droite comme GI.
En effet, sinon pourquoi avoir annoncé à la suite de ce coup médiatique le renforcement de la surveillance aux frontières ?
Pourquoi avoir laisser des militants fascistes déployer leur propagande nauséabonde ?
L'assymétrie des mesures prises par le gouvernement révèle son virage à droite, et l'absence totale d'actions contre ce groupe d'extrème-droite est terrible.

L'opération a été un véritable succès pour Génération Identitaire.
Iels ont réussi un coup médiatique sans bavures, n'ayant pas eu à recourir à la violence.
Il n'y avait évidemment pas de migrant·e·s qui se sont aventuré·e·s vers elleux, et les forces de l'ordre se sont tenues à l'écart.
Et ceci donne des articles comme cet [immondice](http://www.lemonde.fr/police-justice/article/2018/04/23/hautes-alpes-deux-militants-interpelles-apres-l-entree-de-migrants-venant-d-italie-dimanche_5289411_1653578.html), qui explique calmement que vraiment c'est terrible de faire entrer des migrant·e·s dans notre charmant pays.
Il faut aussi (et surtout) souligner que cette démonstration s'est faite en pleins débats concernant la loi asile et immigration, de quoi donner quelques preuves de soutien envers les conservateurices.
Le coup total de l'opération est d'ailleurs assez élevé (location de deux hélicoptères, drones...), pour un total de 30 000€.
