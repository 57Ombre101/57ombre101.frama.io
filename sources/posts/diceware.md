Title: Diceware
Date: 2016-04-05 10:03:00
Authors: Mondrak Schwertmann
Tags: securite
      mot de passe
      informatique

J'ai récemment utilisé pour la première fois le <strong>Diceware</strong>. Je pense que c'est une pratique a partager, surtout en les temps qui courent où notre sécurité est menacée par un état policier.

<strong>Qu'est-ce ?</strong>

Diceware est un outil, un protocole de génération de mots de passes. Le principe est simple : avec des tirages de dés successifs, l'utilisateur détermine aléatoirement plusieurs mots, constituant une <strong>phrase de passe</strong>. 

En effet, il apparaît qu'aujourd'hui, ce qui détermine la sécurité d'un mot de passe, c'est sa taille. Il ne faut donc plus chercher des mots de passes du genre : « (t^!H# » (j'ai généré ce mot de passe avec un générateur pseudo-aléatoire, en incluant majuscules, caractères spéciaux...), mais plutôt : «cemotdepasseestlongetsolideetoui».
(Illustré ci-dessous)

<a href="http://57ombre101.files.wordpress.com/2016/03/password_strength.png"><img title="password_strength.png" class="alignnone size-full" alt="image" src="http://57ombre101.files.wordpress.com/2016/03/password_strength.png" /></a>



C'est pour remédier à ce problème qu'a été créé Diceware ! En effet, cette technique permet de générer de longs mots de passe mémorisables.

<strong>Utilisation</strong>

Pour utiliser Diceware, il faut d'abord une liste de mots sur laquelle se baser. Vous pouvez les retrouver <a href="http://diceware.org">ici</a>.

Ensuite, il vous faut au moins un dé. Soit vous lancez le dé, notez le chiffre tiré, et répétez l'opération cinq fois le nombre de mots que vous désirez, soit vous lancez les dés cinq par cinq, dans une boîte de façon à les prendre toujours dans le même ordre (par exemple en prenant celui dans le coin à droite, puis celui à sa gauche...).

<strong>Pour complexifier</strong>

J'ai dit qu'il fallait tirer cinq mots. Or, c'est partiellement faux. En effet, suivant la sécurité que vous voulez, il vous en faudra éventuellement plus. Par exemple, il est recommandé d'utiliser des phrases de passe de six mots au moins pour GPG, ou le chiffrement de données, et au moins sept mots pour BitCoin ou le chiffrrment de disques entiers. Dans le futur, à mesure que les performances des ordinateurs, et des moyens des attaques de mots de passes augmentent, il faudra des phrases de passe encore plus longues. 

Si le facteur principal pour complexifier un mot de passe généré par Diceware est de rajouter des mots, il est possible d'ajouter des caractères spéciaux. Cependant, ce n'est pas la chose la plus conseillée. 

<strong>Sur mon téléphone</strong>

J'ai voulu testé cette méthode pour faire un mot de passe pour mon ordiphone (parce qu'il n'est pas très smart...). J'ai donc tiré quatre mots. Seulement, je me suis vite rendu compte que mon téléphone n'admet que 16 caractères maximum pour un mot de passe. Et il faut obligatoirement ded chiffres. Alors je n'ai pris que kes deux premiers mots, et j'ai intercalé un nombre tiré au hasard entre les deux. Etant donné qu'il faut qu'il ne soit pas trop long a taper, je n'ai pas inclus de caractères spéciaux. J'ai donc un mot de passe d'une dizaine de caractères, facile à retenir et utilisable.

Pour plus d'informations sur Diceware, consultez le site officiel : là. 


<strong>Bonus</strong>

Bon, là ça ne concerne pas directement Diceware. Simplement, les mots de passe, pour la plupart voire tous, ont un nombre maximum de caractères, noté <em>n. </em>La question que je me suis posé est la suivante : «vaut-il mieux avoir un mot de passe de n caractères, ou un un plus faible (<em>n-1</em>, <em>n-2</em>, ..., <em>2</em>, <em>1</em>)». La réponse semble évidente, pourtant, si le hacker sait que vous êtes du genre à avoir un mot de passe sérieux, il pourrait commencer par chercher les phrases de passe de <em>n</em> caractères, affaiblissant votre défense. Vaut-il donc mieux avoir <em>n-1</em> caractères ?

Posons <em>p</em> le nombre de caractères possibles par case. Soit <em>L</em> la longueur du mot de passe. Le nombre de combinaisons possibles pour une phrase de passe de <em>L</em>caractères est <em>p^L</em>. 
Montrons que <em>p^L &gt; Somme des combinaisons pour des longueurs allant de 1 à L-1</em>. C'est assez instinctif, c'est le même principe qui fais que tout nombre à deux chiffres, avec le premier chiffre différent de 0, est supérieur à 9 par exemple.

Une autre manière de voire les choses, c'est que tout mot de passe de <em>n</em> caractères contient tous les mots de passes de <em>n-1</em>, ... caractères. Ainsi chaque mot de passe de 3, 2 ou 1 caractères peut figurer dans un mot de passe de 4 caractères.

Donc il vaut mieux toujours avoir un mot de passe de longueur maximum, mêmes si votre menace restreindra peut-être ces recherches sur les mots de passe de cette longueur.