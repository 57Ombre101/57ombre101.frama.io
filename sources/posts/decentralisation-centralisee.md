Title: Décentralisation centralisée
Date: 2015-08-21 10:03:00
Authors: Mondrak Schwertmann
Tags: informatique
      libre


Aujourd'hui, nous utilisons de plus en plus d'appareils électroniques «intelligents» : ordinateur à la maison, au boulot, ordiphone, tablette. Or, ce serait pratique de trouver nos données personnelles synchronisées sur tous ces appareils : photos, mails, films, ebook, musique, jeux, calendriers, et j'en passe énormément. C'est d'ailleurs le cas ! Des services se sont mis en place pour palier à ce problème : Google Photos, Flickr, GMail, Google Drive, One Drive, Amazon, Google Music, Apple Store, Steam, Google Play, AppStore, Google Calendar, et j'en passe.

Cependant, certains noms ressortent dans cette liste : Google, Apple, Amazon, et Microsoft. Avec Facebook, ces entreprises sont rassemblées sous l'acronyme de GAFAM. Ce sont des géants du net qui possèdent de plus en plus de nos données. Et lorsque je dis posséder, pour plusieurs de ces services, l'utilisateur accepte de donner la propriété du contenu mis en ligne à l'entreprise. Et oui, ils profitent de ce que la majorité ne lit pas la totalité des Conditions d'Utilisation pour obtenir un droit de possession légal sur les données des utilisateurs. Ce droit n'est pas forcément absolu, et nous gardons un certain contrôle souvent, avec notamment le droit à l'oubli, mais pour l'exercer, il faut parfois exécuter des procédures complexes. Nos données sont donc aux mains d'entreprises qui se les vendent, se les achètent, et ce principalement dans des buts publicitaires.


<strong>Alors décentralisons !</strong>

Pour répondre à ces problèmes, diverses initiatives se sont créées. Je vais principalement évoquer la campagne «Dégooglisons Internet», lancée par <a href="http://framasoft.net">Framasoft</a>. Framasoft défend le libre, et voyant que nous perdions le contrôle des données, a décidé de nous aider à le reprendre. Comment ? En proposant des alternatives aux services des GAFAM. Il y a donc notamment Framapad, qui remplace Google Docs, Framadate pour organiser des événements sans Doodle ou Google Calendar, Framagit afin de se passer de Github. Le but de ces services n'est pas de vaincre Google, un objectif déraisonnable. Il s'agit plutôt de montrer qu'il est possible de vivre sur Internet sans les GAFAM, et de permettre aux utilisateurs de pouvoir migrer leurs données sur un autre service, ou bien de supprimer un compte totalement, choses que n'offrent pratiquement pas ces entreprises. Cette démarche est donc surtout éducative. Framasoft met aussi sur la voie de l'utilisation de services que l'association ne propose pas : les mails par exemple. En effet, j'avais entendu quelqu'un demander : «Quel mail faut-il utiliser ?». La réponse de Framasoft est simple : aucun. En effet, il ne faut pas que tout le monde se rue sur un même mail, entraînant une centralisation, mais que chacun choisisse, selon ses besoins. Framasoft ne peut donc rien conseiller de ce point de vue là. L'association montre les principes que doivent respecter les services : transparence, contrôle des données par l'utilisateur, choix de l'hébergeur (par exemple le choix d'un node Diaspora, d'un serveur XMPP, d'un serveur mail). Ensuite, c'est à l'utilisateur de faire ses choix.


<strong>Mais en centralisant</strong>

Le seul inconvéniant, mais qui peut aussi se révéler un atout en cas de piratage d'un compte, est la dispersion des services : il faut utiliser une multitude de sites, avoir de nombreux comptes (avec un mot de passe différent à chaque fois), si bien que nous pouvons nous sentir un peu perdu.

C'est pour cela qu'il est nécessaire de «centraliser». Cela peut être fait de plusieurs manières : un simple fichier texte avec le nom des services et le nom d'utilisateur associé, un gestionnaire de mot de passe, ou bien, un serveur chez soi. 

En premier lieu, un fichier texte a pour avantage de s'ouvrir facilement. C'est un document toujours accessible, éditable à tout moment. Cependant, il peut être facilement lu par des pirates (ou des entreprises comme Microsoft) qui n'ont qu'a récupérer un petit fichier texte. Cette tâche peut être plus ardue sur un disque crypté, mais présente encore de gros risques. En fait, la principale utilité que je vois de ce système serait de l'utiliser sur une machine en air gap, pour éviter d'installer des logiciels (m'enfin, je ne suis pas sûr que ce soit judicieux d'afficher ses mots de passe si le niveau de sécurité est si élevé).

Concernant les gestionnaires de mots de passe, je commence tout juste à en utiliser. A titre d'information, <a href="http://www.genma.free.fr">Genma</a> utilise <a href="http://keepass.info/">Keepass</a>, qui me semble être un «bon» gestionnaire. En effet, les mots de passes sont présents sous forme chiffrés, accessible uniquement via un mot de passe maître. Ce dernier doit être choisi avec soin et dpit présenter une grande complexité. Vous aurez à vous en souvenir parfaitement. Je suis en train de tester l'application Android KeePassDroid.

Enfin, un serveur chez soi permet un accès physique à la machine. Cela ne résout pas le problème des mots de passes, mais permet d'avoir tous ses services à une même adresse. C'est une solution que je n'ai pas testé (mais bientôt...), mais qui reste relativement simple : il est possible d'obtenir des Raspberry Pi préinstallés, avec des manipulations assez simple à exécuter pour installer des applications basiques. Pour avoir un meilleur niveau de sécurité, il faut se lancer dans des tâches plus complexes (mettre en place un chiffrement SSL, générer des certificats, choses que je ne maîtrise pas, par manque de temps, mais qu'il est largement faisable d'installer).


<strong>Conclusion</strong>

Aujourd'hui, les consommateurs sont victimes de la centralisation et d'une perte de contrôle des données. Heureusement, de plus en plus d'acteurs de l'Internet s'investissent pour offrir des solutions pour décentraliser, et ainsi reprendre le contrôle de leurs données. Ce faisant, ils ouvrent des solutions plus faciles d'accès aux personnes lambda sur la voie de la décentralisation. Comme le dit si bien Framasoft : <blockquote> La route est longue mais la voie est libre…</blockquote>


<u>Sources et liens :</u>

<a href="http://framasoft.net/">Framasoft</a>, lanceur de la campagne «Dégooglisons Internet»
<a href="http://genma.free.fr">Genma</a>, libriste au blog intéressant, que je remercie pour le tuyau sur KeePass.

Pour plus d'informations, n'hésitez pas à me demander.