Title: Rapport de Report From The Interior
Date: 2015-08-30 10:03:00
Authors: Mondrak Schwertmann
Tags: lecture


J'ai lu récemment la biographie <em>Report From The Interior</em> de Paul Auster. Ce livre est le récit de Paul qui raconte la manière dont il voit sa jeunesse, c'est-à-dire de ses premiers souvenirs à ses vingt-cinq ans environ. C'était la première fois que je lisais cet auteur, et je dois dire que j'ai été très intéressé.

<strong>Enfance</strong>

La première partie, <em>Report from the Interior</em>, se focalise sur l'enfance de l'auteur. Il y raconte presque tous ses souvenirs, allant de sa passion pour le sport, ses rencontres avec de grands joueurs de baseball, à ses interrogations sur Dieu, qu'il voit sévère, contrairement au Grand Esprit Indien. C'est une partie assez naïve, ou plutôt innocente, mais en même temps, l'enfant se pose des questions, réfléchit sur le monde et montre certaines absurdités. L'écrivain nous parle de ses premières lectures, Poe, Stevenson, Sherlock Holmes, ainsi que des romans de sport. Il trouve dans tous ces livres des héros qu'il admire. Paul Auster réussit ici à faire une narration habile, assez loin des clichés tout en restant enfantin, c'est-à-dire curieux, malhabile, émerveillé.

<strong>Adolescent</strong>

<em>Two Blows to the Head</em> est la seconde partie, celle sur l'adolescence de Paul. C'est le moment où il regarde des films, comme <em>War Of The Worlds</em>, <em>The Shrinking Man</em> ou <em>The Chain Gang</em>, qui le marqueront beaucoup. L'écrivain raconte ces films de manière de précise, de sorte que sans les avoir lu, il est possible de s'en faire une bonne idée, et de comprendre aisément les sentiments du garçon. C'est aussi le moment où il est jeune, adolescent, et où il écoute du rock, avant de se tourner vers la musique classique. Il raconte certaines perles de sa vie d'adolescent, des choses uniques, qui l'étonnent presque. Pendant ce temps, le garçon continue son cheminement dans la littérature, en lisant, et en écrivant, principalement, presque exclusivement pour le travail scolaire.

<strong>Etudes</strong>

Paul Auster devient étudiant. Dans cette troisième partie, <em>Time Capsule</em>, il relate les lettres qu'il a envoyé à sa chère Lydia alors qu'ils étaient séparés. Ces lettres transcrivent tout d'abord un état assez mélancolique, ou l'auteur est à Paris, et a une épée de Damoclès au-dessus de la tête : l'envoi vers le Vietnam. En même temps, il écrit beaucoup, envisage de devenir réalisateur, et n'a pas encore de réel projet pour son avenir. Il retourne ensuite à New York pendant une grève étidiante à l'Université de Colombia. Après cet événement, son.tempérament est profondément changé : il devient plus gai. C'est dans cette troisième partie qu'il forme ce qu'il sera tout le reste de sa vie.

Au final, j'ai bien aimé ce roman, assez complet, avec des passages politiques, ou plus divertissants. J'ai bien aimé la manière dont il raconte son enfance, sans tomber dans le niai ou le dénigrement. Le reste est tout aussi bien écrit et décrit. C'est un livre à lire.