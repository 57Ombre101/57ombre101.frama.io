Title: Retour d'expérience : Sauvegarder son Android
Authors: Mondrak Schwertmann
Date: 2017-03-26 10:00:00
Tags: Android
      Sauvegarde


Mon téléphone Android a récemment eu un petit pépin. Il s'est mis à redémarrer très souvent, lorsque j'activais le mode hors-ligne, lançait la musique... J'ai donc décidé de faire une remise à zéro, et depuis, il fonctionne parfaitement. Or, qui dit remise à zéro dit perte de données (pas celles sur la carte SD ou dans la mémoire de stockage du téléphone, mais les applications, les paramètres, les contacts...). Le problème étant apparu brusquement, sans signe avant-coureur, je n'avais pas eu le temps de faire une **sauvegarde** totale avant d'exécuter les manipulations de réinitialisation.

Heureusement, je réalise depuis un certain temps des sauvegardes hebdomadaires avec **[OAndBackup](https://f-droid.org/app/dk.jens.backup)**. Cela m'a été fort utile, car j'ai pu ensuite réinstaller bon nombre d'applications, et importer mon agenda par exemple, qui n'est pas synchronisé ailleurs. J'ai finalement perdu très peu de choses : trois SMS et les données de quelques applications, dont *Termux*, occasionnant la perte de mes configurations. Globalement, j'ai pu vérifier que mon système de sauvegarde était plutôt réussi.

Cependant, j'ai eu de la chance que la sauvegarde ait été réalisée le jour même, juste avant que mon téléphone ne bug. Si j'avais perdu six jours de SMS et de données diverses, j'aurais été plus embêté. Au final, rien de grave donc. Mais j'ai compris qu'il me fallait faire des sauvegardes plus fréquemment. J'ai donc mis en place une sauvegarde **quotidienne**, et décidé de créer un script pour copier le contenu de **Termux** sur la mémoire interne (non encore fini). Voyons maintenant comment utiliser **OAndBackup**.


#### Mise en place d'OAndBackup ####

**OAndBackup** est très pratique, mais il y a quelques choses à prendre en compte au préalable. Les sauvegardes réalisées sont stockées sur votre téléphone (vous pouvez choisir l'endroit vous-même). Cela prend de la place (2 Go dans mon cas). C'est donc une solution qui peut être peu pratique lorsque la mémoire est limitée. Dans ce cas, regardez plutôt comment réaliser des sauvegardes avec **ADB**, ou **TWRP**. Je n'utilise pas ces outils pour sauvegarder mes données, je ne détaillerai donc pas la procédure ici. De plus, **OAndBackup** nécessite d'avoir un appareil *root*, et **Busybox**, généralement installé par défaut sur les **Roms** alternatives.

Une fois **OAndBackup** installé depuis le **[F-Droid](https://f-droid.org/app/dk.jens.backup)**, il est pratique de programmer des sauvegardes régulières. Pour cela, il suffit d'aller dans "scheduling", de renseigner la fréquence en jours, l'heure de la backup (qui doit être un moment où votre téléphone est allumé) et le type de backup (dans mon cas, une *custom list*, pour exclure **Termux** qui est trop gros (et qui ne se réinstalle pas), et un *data+apk*. N'oubliez-pas de cocher la case *enable* ! Une fois cela fait, votre téléphone sauvegardera régulièrement applications et contenus comme le calendrier, les SMS... Unc fois cela fait, c'est bon, vous avez un système de sauvegardes régulières fonctionnel.

Attention cependant, l'application n'est pas parfaite. Certaines de mes applications (mais peu) ne se sont pas réinstallées. Essayez de vérifier que les données de vos applications les plus importantes peuvent être chargées.

Je n'ai pas cherché à voir s'il est possible de porter une sauvegarde d'un téléphone à l'autre.


#### Conclusion ####

*Les sauvegardes, c'est vital.* Cependant, **OAndBackup** n'est pas la seule solution. Si vous avez peu de mémoire, il est préférable de sauvegarder directement sur un ordinateur, grâce à **ADB** notamment. Cette sauvegarde est d'ailleurs tout aussi nécessaire. Si vous perdez votre téléphone, ou que la mémoire ne fonctionne plus, la solution dont j'ai parlé ci-dessus est inutile. Il faut absolument que vos données soient sauvegardées en plusieurs endroits.

Pour en savoir plus :
Sur son blog, **[Genma](blog.genma.fr)** parle régulièrement des sauvegardes, et notamment de la *règle des 3-2-1*.
**TWRP**, excellent outil pour tout ce qui est bidouille va proposer le moyen de sauvegarder ses données sur le PC, de manière analogue à *ADB* : [https://www.xda-developers.com/twrp-v3-1-0-will-bring-support-for-backing-up-directly-to-your-pc](https://www.xda-developers.com/twrp-v3-1-0-will-bring-support-for-backing-up-directly-to-your-pc)