Title: Arrêtez d'employer le [censored] de terme hashtag à tort et à travers bande d'[censored] !!!!
Date: 2015-02-28 11:58:00
Authors: Mondrak Schwertmann
Tags: hashtag


Bon, je n'en peux plus, j'en ai marre ! Marre que tout le monde ne sache pas ce qu'est un putain de hashtag ou le nom de signe : #. C'est pourquoi j'ai décidé de vous l'expliquer. En essayant de rester calme.
<h3>Hashtag</h3>
Pour commencer, qu'est-ce qu'un hashtag ? Vous reconnaitrez dans ce mot "hash" et "tag". Commençons par la fin. Vous pourrez retrouver "tag" dans "nametag" sur Minecraft, ou bien sur les murs des villes. En fait, dans ce cas-là, un tag est une sorte de mot-clef (ou expression-clef). C'est un mot (ou une expression) pris à part.
"Hash" quant à lui peut être vu dans "table de hashage" (pour les torrents), c'est un algorithme chargé d'établir un nombre (avec tous les caractères alphanumériques en fait) grâce à des variables d'entrées, ou comme je vous le redirai plus loin, comme le signe "#".
Et "hasthag" alors ? Et bien, c'est un mot-clef (ou expression-clef, je le répète). C'est le <strong>mot</strong>, non <strong>pas</strong> le signe "#" !!!! Ce terme a été rendu populaire grâce à Twitter (http://www.urbandictionary.com/define.php?term=Hashtag&amp;defid=5352593) et est maintenant utilisé à tort et à travers. Mais ça reste une expression-clef (comme #hashtag ou #jeSuisEnRogne ou encore #jeVaisVousFaire<strong>Mal</strong>SiVousContinuez).
<h3>Et # alors ?</h3>
# ? En français, cela se prononce (et s'écrit d'ailleurs) "dièse". C'est un signe utilisé en musique, ou sur les téléphones (je n'ai jamais compris pourquoi d'ailleurs). En anglais, il peut se prononcer "pound" ou "hash", d'où la confusion.
Donc un "hashtag" est composé de "dièse" et d'un mot. Ce n'est pas juste "#", donc arrêtez de dire "o lol hashtag plusBelleLaVie koi mdr !" bordel de merde !!!!!!!!!!