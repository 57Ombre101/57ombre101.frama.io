Title: 3ème Guerre Mondiale
Date: 2013-11-20 20:46:00
Author: Mondrak Schwertmann
Tags: Jeu vidéo

Le <strong>jeu</strong> <strong>3ème Guerre Mondiale</strong>, sous son nom très banal, abrégé 3gm, cache un jeu de <strong>stratégie</strong> <strong>dynamique</strong>, géré par un développeur actif, <strong>Spoke</strong>, et par une bonne équipe de modération.

Ce jeu par<strong> navigateur</strong> vous met aux commandes d'une base, créé par un groupe de survivants de la Troisième Guerre Mondiale. Le jeu intègre un système d'<strong>alliances</strong>, de <strong>marché mondial</strong>, vous permettant de vendre et d'acheter des ressources, mais aussi des unités. Une <strong>bonne entente</strong> s'est instaurée entre les joueurs, et étant donné le relatif faible niveau des joueurs actuellement, du fait du lancement de la <strong>version 1.0</strong>, il peut être judicieux de commencer maintenant...

Au début, je vous conseil de booster vos <strong>ressources</strong>, en augmentant la production de <strong>silicium</strong> et d'<strong>argent</strong>. Construisez-vous des <strong>défenses</strong> petit à petit, et développez des <strong>technologies</strong>. Puis, fabriquez des <strong>troupes</strong> et<strong> attaquez</strong> !

Pour se financer, le jeu propose un <strong>Blackmarket</strong>, qui permet, contre quelques euros d'acheter des bonus. Cependant, ce dispositif ne déséquilibre pas le jeu.

Il est a regretter cependant le <strong>manque de profondeur</strong>. En effet, j'apprécierais grandement avoir plein d'autres améliorations et unités, mais ceci est personnel, et ne sera peut-être pas partagé par tous.

&nbsp;

Site officiel : [www.3gm.fr](https://www.3gm.fr)

Votez pour le jeu : http://www.jeux-alternatifs.com/3e-guerre-mondiale-jeu706_hit-parade_1_1.html