Title: Excalibur, ou la Légende du Roi Arthur et du Graal
Date: 2013-06-14 15:52:00
Authors: Mondrak Schwertmann
Tags: film

###Excalibur, ou la Légende du Roi Arthur et du Graal

Salut à tous, aujourd'hui, je vais vous parler d'Excalibur.

C'est un film portant sur l'histoire de cette épée, à travers l'histoire d'Arthur. Il est sorti en 1981. Réalisé par une équipe américano-britannique.

J'ai trouvé le film très bien. Il commence avec une bataille du père d'Arthur, Uther, contre un rival, le Duc de Cornouailles. Après la défaite du Duc, Uther jure à Merlin, qui l'a aidé, de faire la paix avec le vaincu. Ce faisant, Merlin lui promet de lui donner Excalibur. Mais, Uther désire la femme du Duc, Ygraine (aussi orthographié Igraine). Il attaque donc le château, attire le Duc dans un piège mortel grâce à Merlin. Ainsi, Ygraine devient sa femme...

Ce qui est bien, c'est que le film est très bien construit. Ainsi, la mort du Duc coïncide avec le viol d'Ygraine par Uther, qui a pris l'apparence du Duc. Son agonie - il est tombé sur un ratelier, où se trouvaient plein de lances - est entrelacée avec le viol d'Ygraine. Les deux chevaliers, tous deux en armures, se ressemblent et permettent de jeter une atmosphère morbide sur cette scène, le triomphe d'Uther.

L'histoire n'est pas totalement respectée, puisque ce n'est pas Galahad mais Perceval qui trouve le Graal, et le Roi qui le détient est Arthur, et non le Roi pêcheur.

Bref, bon film, que je vous conseille fortement !

Sources : <a title="Excalibur - Wikipédia" href="http://fr.wikipedia.org/wiki/Excalibur_%28film%29" target="_blank">Wikipedia</a>.