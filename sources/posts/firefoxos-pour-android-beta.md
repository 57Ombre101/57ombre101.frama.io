Title: FirefoxOS pour Android (Béta)
Date: 2015-12-27 10:09:00
Authors: Mondrak Schwertmann
Tags: mozilla
      firefoxos
      android

Bonjour,
J'ai testé récemment, après avoir lu <a href="http://genma.free.fr/?Firefox-OS-pour-Android-Quelques-retours-succincts-par-Matiu">cet article du blog de Genma</a>, FirefoxOS 2.5 pour Android. J'ai testé ce launcher (version 2.6.0.0-prerelease - 20151209110748 - nightly) sur un Samsung Galaxy S3 (sous Rom stock, Android 4.2).

En bref, cet application permet d'installer FirefoxOS, mais pas sous forme de Rom. Elle n'écrase donc pas le système Android, s'installe et se désinstalle facilement. Cette application n'a pas tout à fait le même but qu'une sorte de dual-boot Android/FirefoxOS, puisqu'il s'agit plus de permettre de tester FirefoxOS qu'utiliser FirefoxOS au quotidien. Je vais ici vous parler des défauts que j'ai relevé, et des raisons qui me font quand même aimer cette application.

Concernant la liste des «problèmes», je vais tout simplement copier une partie de mon commentaire sur le post de Genma (c'est comme ça, je suis assez fainéant :
<blockquote>
- Il met un peu de temps à démarrer. Ce ne serait pas dérangant si il ne redémarrait pas assez souvent ;
- Cette lenteur se retrouve à différents niveaux (sur les apps ’FirefoxOS’) ;
- L’application "Paramètres" à une réelle tendance à planter (mais ça dépend, tout comme la plupart des apps) ;
- La carte SD n’est pas reconnue ;
- La carte SIM non plus ;
- Pour les icônes Home/Retour/Options d’Android, il m semble qu’elles fonctionnes assez aléatoirement (le bouton home tout le temps par contre) ;
- Le lockscreen proposé rencontre des difficultés avec celui de base (qui a un mot de passe pour ma part) ;
- Le déplacement des icôns est un peu fastidieux si l’icône déplacée est en bas de la liste, il faudrait des règles de tri ;
- Pas de support de widgets ou de fond-d’écran animé ;
- Les icônes vers le bas de l’écran d’accueil ont du mal s’afficher.
</blockquote>

J'ajouterais aussi des défauts liés à la cohabitation avec Android :
 - les problèmes des boutons physiques/virtuels (évoqués précédemment)
- le volet descendant : parfois c'est celui d'Android, parfois celui de Firefox

Ceci sont les principaux défauts que j'ai remarqué. Il y en a certes pas mal, mais ce n'est qu'une version de test, donc c'est amené à changer (je testerai les futures versions de même). 

Pour moi, ce launcher est une bonne nouvelle de la part de Mozilla. En effet, il permettra à n'importe qui de tester facilement FirefoxOS, sans avoir besoin de rooter le téléphone, chercher un build qui marche... De plus, une fois la stabilité améliorée, ce sera possible d'utiliser cette application de façon permanente, et de ne presque plus repasser par la couche de base d'Android. C'est donc un nouveau jalon sur la voie du Libre, qui s'inscrit dans la campagne de Foxfooding et qui malgré certains faits (arrêt des partenariats Mozilla/constructeurs), montre que le renard en a encore dans le ventre !