Title: Un wiki personnel avec TiddlyWiki
Date: 2016-10-29 09:00:00
Authors: Mondrak Schwertmann
Tags: wiki
      informatique


Récemment, je me suis mis à la recherche d'un wiki personnel. Pourquoi ? Cet outil permet de rassembler différentes connaissances (citations intéressantes, culture scientifique...), techniques (séries de lignes de commandes pratiques, marches à suivre pour sauvegarder certaines données, solutions à divers problèmes rencontrés...) très pratiques. Il existe de nombreuses manières de créer un wiki personnel : avec <a href="https://www.dokuwiki.org/dokuwiki#">Dokuwiki</a>, Zim ... Pour ma part, j'avais quelques critères spécifiques :
 * un wiki disponible sans besoin de serveur
 * un wiki manipulable sous Android et Linux

Trois ont retenu mon attention :
 * <a href="http://zim-wiki.org/">Zim</a>
 * <a href="http://stickwiki.sourceforge.net/">Wiki On A Stick</a>
 * <a href="http://tiddlywiki.com/">TiddlyWiki</a>
Cependant, je n'ai pas trouvé l'application Android de Zim. Certes, il y a <a href="https://github.com/hansihe/ZimDroid">ce dépôt Github</a> qui devrait me permettre d'en compiler une, mais bon, ce n'était pas très engageant. Wiki On A Stick avait l'air plus prometteur. Il suffit d'un simple fichier HTML, la syntaxe est claire (peut-être rebutant au début, mais très proche de la syntaxe Wikipédia). Là aussi, j'ai quand même rencontré der problèmes : malgré l'essai avec tous les navigateurs de mon téléphone, je n'ai jamais réussi à enregistrer mes modifications. Cependant, ça marchait à peu près sur l'ordinateur.


<strong>Premiers pas avec Tiddlywiki</strong>

Vous l'aurez sûrement deviné grâce au titre, Tiddlywiki fut mon choix final. En effet, c'est la seule solution qui répondait à mes critères. De plus, c'est une solution modulable, fiable (quelqu'un a annoncé avoir retrouvé un vieux wiki datant de 2009 fonctionnant encore, ce n'est pas forcément le cas de tous les logiciels !) et qui a de multiples usages.
Pour l'installer, suivez les instructions ici : http://tiddlywiki.com/. En principe, il faut d'abord un fichier de base, que vous pouvez retrouver sur la même page. Le reste de la procédure varie selon les supports sur lesquels vous consultez le wiki. Ensuite, je vous laisse découvrir, le fonctionnement est assez simple. Vous renseignez d'abord les informations sur le wiki, puis vous pouvez commencer à créer des pages : les <em>tiddlers</em> et à les éditer. Des plugins sont disponibles, cependant, je ne les ai pas encore testés.


<strong>Créer une page Catégorie</strong>

Rapidement, j'ai voulu obtenir des pages listant tous les <em>Tiddlers</em> d'un certain Tag. Or, il n'existe pas de fonction presse-bouton pour ça. Cependant, il suffit d'un petit script à mettre sur votre page listant les <em>tiddlers</em> voulu :
[code language="css"]
&lt;&gt;
[/code]


<strong>Pour aller plus loin :</strong>

Je n'ai fait ici qu'aborder les bases. La suite dépend de ce que vous voulez faire : livre de recettes, carnet de notes quotidiennes, documentation technique... J'ai rassemblé ici quelques liens utiles :
 * Doc utile http://www.phusewiki.org/docs/2006/PD02.pdf
 * Site basé sur TiddlyWiki expliquant les bases, et un peu plus : http://www.giffmex.org/twfortherestofus.html