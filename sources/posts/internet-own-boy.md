Title: The Internet's Own Boy
Date: 2015-01-28 22:34:00
Authors: Mondrak Schwertmann
Tags: aaron swartz
      informatique
      libre

J'ai récemment regardé le documentaire <em>The Internet Own's Boy</em>, relatant la vie d'Aaron Swartz. Il est possible que vous ne saviez pas qui c'est, et je m'en vais vous l'expliquer.

Aaron Swartz est l'inventeur à 14 ans d'une sorte de Wikipédia, pour lequel il gagne un prix Ars Digita, avant la sortie de la célèbre encyclopédie en ligne (il contribuera par la suite à Wikipedia), et un contributeur au projet des flux RSS au même âge. Les autres membres du projet ne se doutaient même pas qu'ils avaient affaire à un enfant. Aaron Swartz était un génie précoce, curieux et désireux d'aider les autres. Il travailla aussi sur web.py, et est l'un des fondateurs de reddit.com. Il a contribué à de multiples projets, et grâce à la vente de Reddit, est rapidement devenu millionnaire. 

<strong>Creative Commons et engagement dans l'open-source</strong>

En plus de compétences techniques, Aaron Swartz a tenté de créer un monde plus libre, où les informations seraient partagées entre tous. Il a alors été un rédacteur actif de Wikipedia, a publié sur WikiLeaks, et a été un moteur du développement des Creative Commons.  Ce type de licence permet d'enregistrer des œuvres simplement, en choisissant quels droits leur attribuer (modification, partage avec ou sans les mêmes droits...). Aujourd'hui, les CC sont couramment utilisée. Le documentaire cite l'exemple de Flickr, et pour ma part, les quelques morceaux que j'ai posté sur Soundcloud sont sous Creative Commons. Cette licence, maintenue par une équipe de bénévoles, est aujourd'hui assez couramment utilisée. Et cela a été rendu possible grâce à plusieurs personnes, dont Aaron, qui au cours de sa vie, s'est montré de plus en plus en plus intéressé par le droit et la politique.

<strong>JSTOR, le MIT, et une justice à l'américaine</strong>

Aaron, âgé d'une vingtaine d'années, entre au MIT, prestigieuse école, où crocheter des serrures est obligatoire. Il a là-bas accès à la base de données JSTOR, qui rassemble des articles scientifiques, pour un prix plutôt élevé, mais ici payé par l'université. C'est une chance pour les étudiants, qui peuvent consulter des articles sur de multiples sujets. A plusieurs reprises, Aaron tente de télécharger de nombreux documents depuis cette base. Il finit par brancher son ordinateur directement au point d'accès, et télécharge des milliers d'articles sur un disque dur externe. Cependant, la police s'en aperçoit, et installe dans le local une caméra de surveillance. Elle finit par filmer le coupable. Le soir même, en rentrant de l'université à vélo, Aaron se retrouve pourchassé par la police, sans comprendre. Il est emmené au commissariat où il est maltraité. Il est libéré après garde à vue, et pendant deux ans, il attendra son jugement. Que risquait Aaron Swartz ? Le tribunal pénal l'a au départ chargé de 4 chefs d'accusation, pouvant conduire à 35 ans de prison, mais au fur et à mesure, d'autres chefs d'accusation ont été déposé contre le jeune homme. Il pouvait donc être condamné jusqu'à 50 ans de prison et 1 million de dollars d'amende. Sa famille a révélée avoir dépensé plusieurs millions de dollars en frais judiciaires. Ce procès a permis de montrer des points pervers, ou faibles du système judiciaire étasunien. Le fait de recourir à la justice pénale, de menacer autant quelqu'un qui n'a pas encore commis de faute (il avait le droit de télécharger les documents, mais pas de les partager, ce qui était probablement son but), ou une faute assez légère (partager le savoir) est tout simplement monstrueux. 
Suite à ces accusations, à quelques jours du verdict, le 11 Janvier 2013, Aaron Swartz se suicida, victime d'un système judiciaire impitoyable.

<strong>Quels leçons pouvons-nous en tirer ?</strong>

Notre société n'est pas encore préparée face aux nouvelles technologies. Les lois doivent maintenant prendre en compte l'Internet, et les sanctions doivent être à la mesure des actes. Un meurtre doit toujours être plus grave que le partage. 
Je suis pour la diffusion du Savoir envers tous, et j'ai beaucoup de mal à comprendre comment quelqu'un peut être accusé de diffuser le savoir. Je défends l'Humain avant l'économique, qui aujourd'hui est pourtant dominant, et instaure des règles stupides.
Aaron nous a légué un héritage important. Il a pu montrer qu'il est possible de s'opposer à des lois comme la SOPA. Il a participé qux fondements d'outils maintenant presque indispensables. Au vu de son habileté politique, technique, et de sa grande intelligence, Aaron aurait pu totalement transformer le monde, si une justice bornée et tortionnaire ne s'était pas dressée devant lui.

Sources :
 - <em>The Internet Own's Boy</em> (Je vous recommande fortement de regarder ce film très intéressant.)
 - <em>Wikipedia</em>