Title: Universal War One
Date: 2016-06-17 10:24:00
Authors: Mondrak Schwertmann
Tags: lecture
      bande-dessinee

C'est d'une bande dessinée dont je vais parler aujourd'hui : <strong>Universal War One</strong> de <strong>Denis Bajram</strong>. En effet, j'étais à la bibliothèque, et je suis tombé sur cette bande dessinée en 6 volumes. Au départ, je m'attendais à un scénario de science-fiction assez standard, avec des gentils terriens défonçant joyeusement des aliens, comme c'est malheureusement si souvent le cas. Cependant, il est apparu que <strong>Universal War One</strong> est plus que cela. En effet, le scénario est intéressant, il y a un bon aspect scientifique, de bons éléments originaux. 

<strong>Un court résumé sans trop dévoiler</strong>

En l'an 2098, la Terre n'est plus gouvernée par une multitude de pays, mais par une fédération, dont le bras armé se nomme <strong>United Earthes Force</strong>. Le pouvoir de cette structure ne peut être contrée que par le CIC, consortium d'entreprises très puissantes. Les humains ont colonisés le système solaire grâce à des progrès techniques comme des modules anti-gravité. Depuis quelque mois, le Mur, immense barrière noire est apparu aux environs de Saturne. L'escadrille Purgatory va tenter de comprendre ce qu'il se passe, affrontant ennemis et énigmes scientifiques. Leur périple sera éprouvant, et les amènera à percer de grands mystères de la science.

<strong>Un scénario plutôt bon et de beaux dessins</strong>

Tout d'abord, le scénario est assez bien ficelé. En effet, il n'est pas tout à fait classique, et l'issue finale demeure ouverte jusqu'au bout. Je n'ai jamais suspecté de tels rebondissements, alors qu'ils restent logiques et cohérents, ce qui n'est malheureusement pas donné à toutes les séries. Ce n'est pas une intrigue que l'auteur cherche à faire durer, mais bien une histoire dans son ensemble. Je ne peux guère dire plus pour ne pas vous dévoiler la suite. Voyez par vous-même, ça en vaut la peine.
Les personnages sont intéressants, et présentent de sombres passés qui nuancent leurs actions. Il y a un violeur qui tente de résister à ses pulsions, un soldat trop courageux qui empire les accidents par ses actions, son camarade lâche au plus haut point qui a vu sa vie brisée par un mariage, un scientifique brutal, une soldat qui a défiée des ordres inhumains, une autre violée par son propre père dans son enfance, et la fille d'un général puissant, qui essaie de maintenir la cohésion du groupe.
Concernant la qualité graphique, je n'ai rien à redire. Il y a eu usage d'un ordinateur pour les couleurs, et plus précisément pour le dernier volume, mais la patte du dessinateur est clairement là. L'ambiance spatiale dans laquelle sont plongés les personnages est bien rendue. L'utilisation de clair-obscur permet de renforcer le caractère, comique, sinistre ou oppressant, exigé par la scène. Le dessin rend l'ensemble assez dynamique. Cette bande-dessinée a un visuel plutôt bien fait.

<strong>Un aspect scientifique creusé</strong>

L'un des personnages principal, Kalish, est un génie, exclu de l'équipe scientifique de l'armée à cause de son comportement bagarreur, bien qu'il soit apparemment un grand génie. De fait, le côté scientifique est très apparent. Pour une fois, cela ne tombe pas trop dans le côté «<em>Science is bullshit.</em>», où le scientifique est un grand type maigre qui porte des lunettes et a du mal à interragir avec les autres. Kalish sauve quand même le monde à coup de pages d'équations rédigées en une seule nuit.
Certains aspect sont néanmoins assez bien renseignés. En particulier, concernant les paradoxes temporels, l'auteur rappelle différentes théories les concernant :
 — Modifier un événement dans le passé détruirait l'univers ;
 — L'univers est déjà la somme de toutes les interventions par des voyages temporels.
Il manque juste une troisième théorie aujourd'hui aussi avancée par les scientifiques : à chaque voyage, un nouvel univers serait créé. Cependant ce n'est qu'une hypothèse et son omission n'a que peu d'importance.
Kalish, confronté à ces hypothèses, a du mal à décider laquelle est la bonne. Forcément, il y a le risque de détruire l'univers qui l'embête. Il pourrait à un moment faire un voyage dans le temps, mais pense que ce risque, la destruction de l'univers, est trop grand. Il s'avèrera par la suite que fort heureusement, le monde n'explose pas à cause d'un saut dans le temps. C'est je pense le plus probable, s'il est possible de voyager dans le temps. Ce qui se réalise est le fait que le continùm espace-temps est cohérent avec lui-même. C'est-à-dire, par exemple, quelqu'un qui écoute une musique, remonte dans le temps et fais écouter le morceau à son compositeur avant la composition, lui donnant alors l'idée. Du coup comment le morceau a-t-il été inventé «la première fois» ? J'aurais aimé voir peut-être abordé l'explication plus en détails, mais ça n'aurait plus été de la science-fiction, mais un manuel de physique.
Les explications scientifiques ne sont pas sans failles ni lacunes. Par exemple, le théorème de Landstadt, permettant la création d'anti-gravité, n'est bien évidemment pas explicité.  
Un phénomène clef reste cependant impossible : la découpe d'une planète en deux. En effet, le découpage d'une planète ne la fait pas se séparer, en tout cas pas si elle est découpée par un faisceau large d'un nanomètre. Pour expliquer pourquoi, j'ai fait un petit schéma :
<a href="http://57ombre101.files.wordpress.com/2016/05/s-mc3a9mo_01-jpg.jpeg"><img title="S Mémo_01.jpg" class="alignnone size-full" alt="image" src="http://57ombre101.files.wordpress.com/2016/05/s-mc3a9mo_01-jpg.jpeg" /></a> 

Nous pouvons voir que ce qui assure la cohésion de la matière n'est pas un ensemble d'interactions de contact, mais plutôt l'ensemble des attractions gravitationnelles sur chaque particule composant la planète. Donc, si un trait (ou plutôt un plan en fait dans notre cas) était tracé en découpant la Terre en deux, la cohésion serait encore assurée. Cela n'aurait sans doute pas d'autres conséquences que la destruction partielle des bâtiments les plus hauts, et la découpe des êtres vivants sur le passage. Cependant, étant donné le mode de fonctionnement de cette machine, les dégats peuvent être évités ou limités. Il n'y a donc pas lieu de le craindre.


<strong>Distance avec la science-fiction actuelle</strong>

Quelque chose qui m'a surpris, c'est que cette bande-dessinée est différente des scénarios de science-fiction actuels. Certes, c'est un groupe de soldats qui va sauver le monde, mais ces personnes ne sont guère stéréotypées. L'image du héros américain disparais assez vite. Le seul personnage de ce genre, téméraire, ancien quarterback, est peu présent de l'histoire. Les autres personnages sont moins beaux, avec plus de défauts, moins parfaits que ce que nous pouvons voir dans des films comme <em>Les Guardiens De La Galaxie</em> ou les comics standard. Kalish notamment, a un caractère très marqué. C'est un libertaire, génie scientifique, bagarreur, qui tente de comprendre la science, et est parfois effrayé par les possibles conséquences de ses découvertes.


<strong>Conclusion</strong>

<strong>Universal War One</strong> est une très belle bande-dessinée à l'histoure intéressante. J'ai aimé du début à la fin. C'est une œuvre qui intéressera les amateurs de science-fiction.