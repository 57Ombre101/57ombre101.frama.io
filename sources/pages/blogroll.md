Title: Blogroll

##Ailleurs sur le web

###Informatique, vie privée

 * [La Quadrature du Net](https://www.laquadrature.net/) : Association française de défense des droits sur Internet, membre des Éxégètes Amateurs
 
 * [Le blog de Genma](https://www.genma.fr/blog/ "Le Blog de Genma") : Un blog tenu par un évangeliste français du libre, [Genma](www.genma.fr).
 
 * [Angristan](https://www.angristan.fr) : un jeune libriste sysadmin qui a de nombreuses choses intéressantes à dire.
 
 * [Cyber-home of lanodan](https://hacktivis.me) : Le site de lanodan, libriste, queer, torroriste convaincu•e, auquel vous pouvez accéder via Tor à [cette adresse](moudbpmicyvvmhoh.onion). J'ai récupéré pas mal de choses en lisant le code source de son site.

 * [Tuxicoman](https://tuxicoman.jesuislibre.net) : Encore un libriste...


###Genre, sexalité

 * [Mrtino](www.mrtino.eu) : Une jeune trans parle un peu de sexualité, mais aussi d'Internet, de nos libertés...


###Tor (ou Darknet si vous êtes tr0 d4rk)
 * ~~[Cyber-home of lanodan](https;//moudbpmicyvvmhoh.onion) : Voir plus haut.~~ Addresse abandonnée
 * [Genma]() : Voir plus haut
 * [Extrémiste Inside (aeris)](http://aeriszyr4wbpvuo2.onion)
 * [Blog Stéphane Bortzmeyer](http://7j3ncmar4jm2r3e7.onion)
 * [The Pirate Bay](http://uj3wazyk5u4hnvtk.onion)
 * [DuckDuckGo](http://3g2upl4pq6kufc4m.onion)
 * [The Hidden Wiki](http://zqktlwi4fecvo6ri.onion/wiki/index.php/Main_Page)