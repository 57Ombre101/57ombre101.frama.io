Title: FOSS
Authors: Mondrak
Date: 2017-05-22


*Inspiré de <https://hacktivis.me/librisme>.*

## Services utilisés ##

|Service propriétaire| Status |Commentaires                                       |
|--------------------|--------|---------------------------------------------------|
|Youtube             |Enchaîné| Pas d'alternative utilisée pour le moment         |
|GMail               | Moyen  | Un compte chez [mailoo](https://mailoo.org/), une autre addresse chez Google qu'il me faut migrer ; Utilisation du client Thunderbird|
| Window$            | Bon    | Un PC sur deux sous Debian, l'autre sous Ubuntu, utilisation de VM Win  |
| Android            | Bon    | ResurrectionRemix (LineageOS)                     |
| Google App$        | Perfect|**fuck that shit**, je les ai oblitérées il y a quelques années|
| Google Play $tore  |  Bon   | GPlaydownloader de Tuxicoman et Yalp Store        |
| Chrome/Edge/...    |  Bon   | Midori, **Tor Browser**, Brave, Firefox, Chromium, Otter Browser (ça dépend du moment)|
| Compte M$          |Attente | À supprimer dès que possible                      |
| Facebook/Twitter   | Moyen  | Reste un compte pas vraiment utilisé              |
| Dropbox            | Bon    | Plus utilisé, apparemment supprimé par l'entreprise (inactivité|
| Google             |Enchaîné| Voir GMail et YouTube                             |
| M$ $kype           |Mauvais | Compte à supprimer (avec M$)                      |
| Tumblr             |Perfect | Supprimé                                          |
| Deviantart         |Mauvais |                                                   |
| Imgur              |Perfect | Supprimé le 2017-05-23                            |
| Github             |Enchaîné| Pour le moment, je garde. J'ai Framagit en principal|
| Steam              |Mauvais | Je garde pour le moment.                          |
| Wattpad            |Enchaîné|                                                   |
| Wordpress          |Enchaîné|                                                   |
| Aeria Games        | Bon    | Demande de suppression envoyée le 2017-05-23      |
|Planetside 2        |Mauvais | A supprimer                                       |
|Sony Online Untertainment|Mauvais| A supprimer                                   |
| Duolingo           |Enchaîné|                                                   |

Comme on peut le voir, ce n'est pas très avancé...


### Applications Android ###

J'utilise [AppsToOrg]() pour trier mes applications selon leur degré de *liberté*.

| Service | Status | Commentaires                                                 |
|---------|--------|--------------------------------------------------------------|
| Store   | Bon    | F-Droid + Yalp Store                                         |
| Mail    | Bon    | K-9 Mail + OpenKeyChain pour GPG, application mail de base   |
|Navigateur|Perfect| Orfox + Brave                                                |
|Fichiers |Perfect | Un truc libre                                                |
| SMS     | Bon    | Silence                                                      |
| Chat    | Moyen  | Discord, Riot, Telegram, Zom                                 |
| Musique | Bon    | Application Lineage et Quicklyrics                           |
|Pare-feu | Bon    | AFWall+                                                      |
|Sauvegardes|Parfait| oandbackups                                                 |
| Jeux    | Moyen  | anDGS, Apple Flinger, Duel Quizz, GBA Emulator               |
| Mots de passe|Bon| KeepassDX, KeePassDroid                                      |

