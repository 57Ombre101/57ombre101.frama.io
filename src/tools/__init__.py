

def is_markdown(file):
    """Check wether the file is due to be converted

Criteria :
finishes with '.md'
"""
    return file[-3:] == ".md"
