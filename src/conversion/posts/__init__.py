import time as t
import os
import markdown as md
from tools import is_markdown
from conversion import get_html

def conv_post(file):
    """Take the text of a and return a tuple (date, html)

    Structure of the post :

    yyyy-mm-dd hh:mm-ss

    {Markdown content}
    """
    html, meta = get_html(file)

    d_txt = meta["date"][0]
    while d_txt == "" or d_txt[0] == "#":
        d_txt = lines.pop(0)
        # convert date str to something better understandable for Python
    d_txt = d_txt.strip(" \n\t")
    time = t.strptime(d_txt, "%Y-%m-%d %H:%M:%S")
    meta["date"] = time

    return html, meta


def get_all_posts():
    """ """
    post_files_tmp = os.listdir("sources/posts/")
    l_posts = []
    for file in post_files_tmp:
        if is_markdown(file):
            html, meta = conv_post("sources/posts/" + file)
            date = meta['date']
            l_posts.append((date, html, meta)) #date is duplicate to be sort after
    return l_posts
    
def import_latest_entries(num_latest_entries):
    """Return a list of html str with the latest num_latest_entries posts."""
    
    l_posts = get_all_posts()
    l_posts.sort(reverse=True)
    #print(num_latest_entries)
    #print(l_posts)
    return l_posts[0:num_latest_entries]


