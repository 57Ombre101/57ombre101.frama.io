import markdown as md
import os
import time as t
from configuration import num_latest_entries
from conversion.posts import import_latest_entries
from conversion import get_html, format_head


def conv_index():
    """Generate the index.html file"""
    # first a str which is the result of markdown
    html_tmp, meta_tmp = get_html("sources/content.md")
    
    # then completed with <html>, <head>...
    begin = format_head(get_html("sources/head.md")[0], { "title" : ["Home"]}) + get_html("sources/header.md")[0]
    end = get_html("sources/footer.md")[0]
    
    # then inserts the posts
    latest_entries = import_latest_entries(num_latest_entries)
    list_html = []
    for (date, html, meta) in latest_entries:
        time_str = t.strftime("%Y-%m-%d", date)
        list_html.append("<article>\n" + md.markdown('###' + meta["title"][0] + """\n*""" + time_str + "*\n") + html + "</article>\n")
    posts = """\n""".join(list_html)
    html_bis = begin + html_tmp + posts + end
    with open("public/index.html", "w") as f:
        f.write(html_bis)


conv_index()
