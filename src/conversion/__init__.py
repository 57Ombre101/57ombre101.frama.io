"""Conversion module"""

import markdown

def get_html(file):
    """Open the file, and return the html version of it. """
    #print(file)
    with open(file, "r") as f:
        txt = f.read()
    md = markdown.Markdown(extensions = ['markdown.extensions.extra', 'markdown.extensions.meta'])
    html = md.convert(txt)
    try:
        #print(md.Meta)
        return html, md.Meta
    except AttributeError: #if no metadata
        return html, None

def format_head(html, meta):
    [page_title] = meta['title']
    return html.format(title = "Mondrak - " + page_title)
