import os
from tools import is_markdown
import markdown as md
from conversion import get_html, format_head

exceptions = ["sources/footer.md", "sources/head.md", "sources/header.md", "sources/content.md"]


def get_pages(folder):
    """Get the list of pages in folder"""
    tmp = os.listdir(folder)
    pages = []
    for x in tmp:
        if is_markdown(x): #is_markdow bugged
            pages.append(folder + x)
    #print(folder + "$" + str(pages))
    #print("GP : " + folder + " $ " + str(pages))
    return pages

def get_folders(folder):
    """Get the subdirectories of the parent 'folder'."""
    tmp = os.listdir(folder)
    f_list = []
    for x in tmp:
        y = folder + x + "/" #each folder ends w/ a "/"
        try:
            os.listdir(y) # means it is a dir
            f_list.append(y)
            #print("G_F : " + y)
        except NotADirectoryError:
            pass
    return f_list

def get_pages_rec(folder):
    pages = get_pages(folder)
    f_list = get_folders(folder)
    #print(f_list)
    for x in f_list:
        pages_bis = get_pages_rec(x)
        for y in pages_bis:
            pages.append(y)
    #print("G_P_rec : " + folder)
    return pages
    

def conv_page(file):
    """Convert the page 'file' to a html file."""
    print("Page : " + file) #for testing purposes
    #read the file
    html, meta = get_html(file)
    
    # add head, header, footer
    head = get_html("sources/head.md")
    
    begin = format_head(head[0], meta) + get_html("sources/header.md")[0]
    end = get_html("sources/footer.md")[0]
        
        
    #generate html path
    # take file, cut the 'sources/'
    # split on '/'
    path_tmp = file[8:].split('/')
    html_path ="public/" +  file[8:-3] + ".html" #"/".join(path_tmp)
    fold_path = "public/" + "/".join(path_tmp[:-1])
    try:
        os.makedirs(fold_path)
    except: #folder already exists
        pass
    
    #write the html
    with open(html_path, "w") as f:
        f.write(begin + html + end)

def conv_all_pages():
    """Convert all the pages"""
    pages = get_pages_rec('sources/')
    #print("Pages to conv : " + str(pages))
    for p in pages:
        #print("Pages conv : " + p)
        if not(p in exceptions):
            conv_page(p)
