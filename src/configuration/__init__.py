
def get_conflist():
    with open("config", "r") as f:
        conflist_tmp = f.readlines()
    conflist = []
    for x in conflist_tmp:
        y = x.strip(" \t\n")
        #then strip the blanks, /t and /n and leave comments
        if y != "" and y[0] != "#":
            z = y.split(":")
            conflist.append((z[0].strip(" \t\n"), z[1].strip(" \t\n")))
    return conflist

def get_num_latest_entries(conflist):
    """Get the number of latest entries to display"""
    for param in conflist:
        if param[0] == "latest-entries":
            num = param[1]
    return int(num)

conflist = get_conflist()
num_latest_entries = get_num_latest_entries(conflist)
